#!/bin/env python3

import sys
import h5py
from matplotlib import pyplot as plt
import seaborn as sns

for i in range(1, len(sys.argv)):

    try:
        soubor = sys.argv[i]

        f = h5py.File(soubor, 'r')
        list(f.keys())
        matice = list(f.keys())[0]
        dset = f[matice]
        #dset = f['p']
        plt.figure(soubor)
        sns.heatmap(dset[0])
    except:
        continue
    
plt.show()



