#!/bin/env python3

import sys
import h5py
from matplotlib import pyplot as plt
import seaborn as sns

for i in range(1, len(sys.argv)):

    soubor = sys.argv[i]

    #f = h5py.File(soubor, 'r')
    #list(f.keys())
    #dset = f['p']
    #plt.figure(soubor)
    #sns.heatmap(dset[0])
   
    try:
        F = open(soubor, 'r')
    except:
        continue

    lines = F.readlines()

    A = []
    
    for L in lines:
        p = []
        t = L.split(" ")
        t.pop()
        for f in t:
            p.append(float(f))
        A.append(p)

    plt.figure(soubor)
    #plt.imshow(A, cmap='hot', interpolation='nearest')
    sns.heatmap(A)

plt.show()



