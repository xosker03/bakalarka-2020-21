#!/bin/bash

TARGET="128 256 512 1024 2048 4096 8000 8192"

if [ "$hostname" == "T460p" ]
then
	echo "T460p"
	TARGET="128 256 512 1024 2048" # 4096 8000 8192"
fi

rm -vr "./database/data_out/*"


echo $(hostname) > ./database/data_out/info.txt
echo "" >> ./database/data_out/info.txt
lscpu >> ./database/data_out/info.txt
echo "" >> ./database/data_out/info.txt
free -h >> ./database/data_out/info.txt
echo "" >> ./database/data_out/info.txt
lspci | grep -i nvidia >> ./database/data_out/info.txt
echo "" >> ./database/data_out/info.txt

rm -r "./data"

make gpu

for i in $TARGET
do
	echo $i
	
	cp -r "database/$i/data" "./"
	./heatsim 2>> ./database/data_out/time_acc.txt
	mkdir "./database/data_out/${i}_acc"
	mv "./data/data_output" "./database/data_out/${i}_acc/"
	rm -r "./data"

done

make cuda

for i in $TARGET
do
	echo $i
	
	cp -r "database/$i/data" "./"
	./heatsim 2>> ./database/data_out/time_cuda.txt
	mkdir "./database/data_out/${i}_cuda"
	mv "./data/data_output" "./database/data_out/${i}_cuda/"
	rm -r "./data"

done



#(cd microbench && ./bench.sh)
#
#mkdir "./database/data_out/res"
#mkdir "./database/data_out/res2"
#mv "microbench/results" "./database/data_out/res/"
#mv "microbench/results2" "./database/data_out/res2/"

