/**
 * @file      storage.hpp
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Hlavičkový soubor pro práci s hdf5 soubory
 *
 * @version   heatsim 1.0
 *
 * @date      19.5.2021
 */

#ifndef _STORAGE_HPP_
#define _STORAGE_HPP_

#include <string>
#include <complex>
#include "main.hpp"

using FloatComplex = std::complex<float>;

int loadhdf5(int **data, std::string name);
int loadhdf5(float **data, std::string name);
int loadhdf5(FloatComplex **data, std::string name);

void storehdf5(int *data, std::string name);
void storehdf5(float *data, std::string name);
void storehdf5(FloatComplex *data, std::string name);

#endif
