/**
 * @file      cpu_acc.hpp
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Hlavičkový soubor pro cpu_acc.cpp
 *
 * @version   heatsim 1.0
 *
 * @date      19.5.2021
 */

#ifndef _CPU_ACC_HPP_
#define _CPU_ACC_HPP_

#include <complex>
#include "templates.hpp"

using FloatComplex = std::complex<float>;

// HOMOGENOUS

void a_perfusion(int R, int C, float *p_term, float *T, float *kappa,
		FloatComplex *tmpFft, float coef, float temp, float idivider);

void a_after_fft(int N, FloatComplex *tmpFft, float *k, float *kappa);

void a_after_tff(int N, float *tmpTff, float idivider);

void a_d_term(int N, float *d_term, float *tmpTff, float diffusion_p1, float diffusion_p2);

void a_T(int N, float *T, float *d_term, float dt);
void a_T(int N, float *T, float *d_term, float *p_term, float *q_term, float dt);

void a_cem43(int N, float *cem43, float *T, float dt);


// NO hOMOGENOUS
void a_perfusion_noh(int R, int C, float *p_term, float *T, float *kappa,
		FloatComplex *tmpFft, float *coef, float *temp, float idivider);

void a_fft_x_noh(int N, FloatComplex *fft, FloatComplex *tmpFft, FloatComplex *deriv);

void a_tff_x_noh(int N, float *tff, float idivider, float diffusion_p2);

void a_fft_x_2_noh(int N, FloatComplex *fft, FloatComplex *deriv);

void a_pre_d_term_noh(int N, float *tff, float idivider);

void a_d_term_noh(int N, float *d_term, float *diffusion_p1_noh, float *tff_1, float *tff_2);
void a_d_term_noh(int N, float *d_term, float *diffusion_p1_noh, float *tff_1, float *tff_2, float *tff_3);
void a_d_term_noh(int N, float *d_term, float diffusion_p1, float *tff_1, float *tff_2);
void a_d_term_noh(int N, float *d_term, float diffusion_p1, float *tff_1, float *tff_2, float *tff_3);

void a_T_noh(int N, float *T, float dt, float *d_term, float *p_term, float *q_term);

#endif
