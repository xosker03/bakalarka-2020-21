/**
 * @file      main.cpp
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Hlavní soubor Heatsim
 *
 * @version   heatsim 1.0
 *
 * @date      19.5.2021
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>
#include <signal.h>

#include <iostream>
#include <vector>

#include <fftw3.h>
#include <complex>

#if defined (GPU_ACC) || defined(GPU_CUDA)
	#include <cufft.h>
#endif

#ifdef GPU_CUDA
	#include "gpu_cuda.h"
#endif
#include "cpu_acc.hpp"

#include "zaklad/zaklad.hpp"
#include "templates.hpp"
#include "container.hpp"

#include "main.hpp"
#include "helpers.hpp"

#include "timercpp.hpp"
#include "zaklad/timer.h"

using FloatComplex = std::complex<float>;

using namespace std;

//cesta k souborům, které generuje a načítá kWaveDiffsuion
string PATH_INPUT = "./data/data_input";
string PATH_OUTPUT = "./data/data_output";


struct timeval stop, start;

//plány pro FFT
#if defined (GPU_ACC) || defined(GPU_CUDA)
	cufftHandle planR2C;
	cufftHandle planC2R;
#else
	fftwf_plan planR2C;
	fftwf_plan planC2R;
#endif

dimensions FullDim;
dimensions ReducedDim;
struct knownDim kd = {0,1,2};

bool homogenous = true;
bool use_perfusion = false;
int use_q_term = 0;
int dimension_count = 0;
int TIME = 0;
int sim_time = 0;
bool homo_diffusion_p1 = true;

long step_time = 0;
double diameter_step_size = 0;

//vrací součin dimenzí
int get_size(dimensions d){
	int s = 1;
	for(int i = 0; i < 3; ++i){
		s *= d[i];
	}
	return s;
}


//Funkce pro FFT
void fftR2C(float *a, FloatComplex *b){
#if defined (GPU_ACC) || defined(GPU_CUDA)
	#pragma acc data present(a, b)
    #pragma acc host_data use_device(a,b)
	cufftExecR2C(planR2C,static_cast<cufftReal*>(a),reinterpret_cast<cufftComplex*>(b));
#else
	fftwf_execute_dft_r2c(planR2C, a, reinterpret_cast<fftwf_complex*>(b));
#endif
}

//Funkce pro zpětné FFT
void fftC2R(FloatComplex *a, float *b){
#if defined (GPU_ACC) || defined(GPU_CUDA)
	#pragma acc data present(a, b)
    #pragma acc host_data use_device(a,b)
	cufftExecC2R(planC2R,reinterpret_cast<cufftComplex*>(a),static_cast<cufftReal*>(b));
#else
	fftwf_execute_dft_c2r(planC2R, reinterpret_cast<fftwf_complex*>(a), b);
#endif
}

//transformuje data objektu GpuMem z plné do redukované velikosti
static float * full2reducedR(GpuMem<float> * S){
	printf("Redukuji %s\n", S->name().c_str());
	int size = ReducedDim[0] * ReducedDim[1]  * ReducedDim[2];
	float *tmp = (float*) calloc(size, sizeof(float));
	float *source = S->data();
	for (int z = 0; z < ReducedDim[kd.z]; z++)
		for (int y = 0; y < ReducedDim[kd.y]; y++)
			for (int x = 0; x < ReducedDim[kd.x];  x++)
			{
				int c = get_index(x,y,z, ReducedDim);
				int r = get_index(x,y,z, FullDim);
				tmp[c] = source[r];
				assert(c < size);
			}
	S->dangerous_override(tmp, size);
	return tmp;
}
static FloatComplex * full2reducedC(GpuMem<FloatComplex> * S){
	printf("Redukuji %s\n", S->name().c_str());
	int size = ReducedDim[0] * ReducedDim[1]  * ReducedDim[2];
	FloatComplex *tmp = (FloatComplex*) calloc(size, sizeof(FloatComplex));
	FloatComplex *source = S->data();
	for (int z = 0; z < ReducedDim[kd.z]; z++)
		for (int y = 0; y < ReducedDim[kd.y]; y++)
			for (int x = 0; x < ReducedDim[kd.x];  x++)
			{
				int c = get_index(x,y,z, ReducedDim);
				int r = get_index(x,y,z, FullDim);
				tmp[c] = source[r];
				assert(c < size);
			}
	S->dangerous_override(tmp, size);
	return tmp;
}

//transformuje data objektu GpuMem z konstanty do pole těhto konstant o plné velikosti
static float * homo2nohomo(GpuMem<float> * S){
	printf("duplikuji %s\n", S->name().c_str());
	if (S->size() == 1){
		int size = get_size(FullDim);
		float source = (*S)[0];
		float *tmp = (float*) calloc(size, sizeof(float));
		for(int i = 0; i < size; ++i){
			tmp[i] = source;
		}
		S->dangerous_override(tmp, size);
		return tmp;
	}
	return S->data();
}

//statistiky při výpočtu
void sim_info(void){
	static int loader = 0;
	int t = sim_time;
	printf("\r%s", bc.GREEN);
	printf("Simulation step %d / %d  ", t, TIME);
	printf("%s[", bc.BLUE);
	
	int len = 100;

	int p1 = t * 100 / TIME;
	int p2 = 100 - p1;

	p1 = len * p1 / 100;
	p2 = len - p1;

	for(int i = 0; i < (p1 - 1); ++i)
		printf("=");
	printf(">");
	for(int i = 0; i < p2; ++i)
		printf(" ");

	printf("]%s", bc.GREEN);

	char pr = '|';
	if(loader == 0)
		pr = '|';
	if(loader == 1)
		pr = '-';
	
	printf("   %d%%   step_time: %ldus  diameter %lfus  %c   ", t * 100 / TIME, step_time, diameter_step_size / sim_time, pr);
	printf("%s", bc.ENDC);

	loader = (loader + 1) % 2;
}

void myexit(void){
	printf("%s", bc.CURSORS);
	printf("%s", bc.ENDC);
}

void closeServerHandler(int sig){
	printf("%s", bc.CURSORS);
	printf("%s", bc.ENDC);
	exit(1);
}

int main(){

	//kvůli statistikám, které mění nastavení terminálu, je potřeba ho resetovat po úspěšném i neúspěšném ukonční programu
	signal(SIGINT, closeServerHandler);
	atexit(myexit);

	//načtení řídících dat
	GpuMem<int> mem_controll	(3,						"controll_mat",		false, true, false, true);
	GpuMem<int> mem_export_dim	(dimension_count,		"export_dim",	false, true, false, true);

	GpuMem<int> mem_Full(3,		"Full",		true, false, false, true);
	GpuMem<int> mem_Reduced(3,	"Reduced",	true, false, false, true);
	FullDim = mem_Full.data();
	ReducedDim = mem_Reduced.data();

	int *controll = mem_controll.data();
	dimension_count = controll[0];
	use_perfusion = controll[1];
	homogenous = controll[2];

	printf("HOMOGENOUS %d\n", homogenous);

	int *tmp_dims = mem_export_dim.data();
	FullDim[kd.x] = tmp_dims[0];
	FullDim[kd.y] = tmp_dims[1];
	FullDim[kd.z] = tmp_dims[2];
	TIME = tmp_dims[3];

	ReducedDim[kd.x] = FullDim[kd.x] / 2 + 1;
	ReducedDim[kd.y] = FullDim[kd.y];
	ReducedDim[kd.z] = FullDim[kd.z];

	//Načítání datových matic
	GpuMem<float> mem_T				(get_size(FullDim),		"T",		true, true, true, true);
	GpuMem<float> mem_cem43			(get_size(FullDim),		"cem43",	true, false, true, true);
	GpuMem<float> mem_k				(get_size(FullDim),		"k",		true, homogenous, false, homogenous);
	GpuMem<float> mem_kappa			(get_size(FullDim),		
			"kappa",	true, true, false, homogenous | use_perfusion);
	GpuMem<FloatComplex> mem_tmpFft	(get_size(ReducedDim),	"tmpFft",	true, false, false, true);
	GpuMem<float> mem_tmpTff		(get_size(FullDim),		
			"tmpTff",	true, false, false, homogenous);
	GpuMem<float> mem_d_term		(get_size(FullDim),		"d_term",	true, false, false, true);
	GpuMem<float> mem_dt			(get_size(FullDim),		"dt",		true, true, false, true);


	GpuMem<FloatComplex> mem_fft_1	(get_size(ReducedDim),	"fft_1",	true, false, false, !homogenous);
	GpuMem<FloatComplex> mem_fft_2	(get_size(ReducedDim),	"fft_2",	true, false, false, !homogenous);
	GpuMem<FloatComplex> mem_fft_3	(get_size(ReducedDim),	
			"fft_3",	true, false, false, !homogenous & (dimension_count == 3));
	GpuMem<float> mem_tff_1			(get_size(FullDim),		"tff_1",	true, false, false, !homogenous);
	GpuMem<float> mem_tff_2			(get_size(FullDim),		"tff_2",	true, false, false, !homogenous);
	GpuMem<float> mem_tff_3			(get_size(FullDim),		
			"tff_3",	true, false, false, !homogenous & (dimension_count == 3));
	GpuMem<FloatComplex> mem_deriv_x(get_size(FullDim),		"deriv_x",	true, !homogenous, false, !homogenous);
	GpuMem<FloatComplex> mem_deriv_y(get_size(FullDim),		"deriv_y",	true, !homogenous, false, !homogenous);
	GpuMem<FloatComplex> mem_deriv_z(get_size(FullDim),
			"deriv_z",	true, !homogenous, false, !homogenous & (dimension_count == 3));
	GpuMem<float> mem_diffusion_p1	(get_size(FullDim),		"diffusion_p1", true, true, false, true);
	GpuMem<float> mem_diffusion_p2	(get_size(FullDim),		"diffusion_p2", true, true, false, true);

	GpuMem<float> mem_p_term(get_size(FullDim), "p_term", true, false, false, true);
	GpuMem<float> mem_perfusion_coeff(get_size(FullDim), 
			"perfusion_coeff",				true, true, false, use_perfusion);
	GpuMem<float> mem_blood_ambient_temperature(get_size(FullDim), 
			"blood_ambient_temperature",	true, true, false, use_perfusion);
	
	GpuMem<float> mem_q_term(get_size(FullDim), "q_term", true, true, false, true);

	//Vytvoření plánů pro FFT
	#if defined (GPU_ACC) || defined(GPU_CUDA)
		cufftPlan3d(&planR2C,
                        static_cast<int>(FullDim[kd.z]),
                        static_cast<int>(FullDim[kd.y]),
                        static_cast<int>(FullDim[kd.x]),
						CUFFT_R2C);
		cufftPlan3d(&planC2R,
                        static_cast<int>(FullDim[kd.z]),
                        static_cast<int>(FullDim[kd.y]),
                        static_cast<int>(FullDim[kd.x]),
                        CUFFT_C2R);
	#else
		{
			GpuMem<float> mem_temp1			(get_size(FullDim));
			GpuMem<FloatComplex> mem_temp2	(get_size(ReducedDim));
			planR2C = fftwf_plan_dft_r2c_3d(FullDim[kd.z],
                                    FullDim[kd.y],
                                    FullDim[kd.x],
                                    mem_temp1.data(),
                                    reinterpret_cast<fftwf_complex*>(mem_temp2.data()),
                                    FFTW_MEASURE); //FFTW_MEASURE //FFTW_ESTIMATE
	
			planC2R = fftwf_plan_dft_c2r_3d(FullDim[kd.z],
                                    FullDim[kd.y],
                                    FullDim[kd.x],
                                    reinterpret_cast<fftwf_complex*>(mem_temp2.data()),
                                    mem_temp1.data(),
                                    FFTW_MEASURE);
		}
	#endif


	//zíkání ukazatelů na data
	float* T = mem_T.data();
	float* cem43 = mem_cem43.data();
	float* k = mem_k.data();
	float* kappa = mem_kappa.data();

	float diffusion_p1 = mem_diffusion_p1[0];
	float diffusion_p2 = mem_diffusion_p2[0];
	float dt = mem_dt[0];

	float idivider = 1.0 / get_size(FullDim);


	FloatComplex* tmpFft = mem_tmpFft.data();
	float *tmpTff = mem_tmpTff.data();
	float *d_term = mem_d_term.data();

	FloatComplex *fft_1 = mem_fft_1.data();
	FloatComplex *fft_2 = mem_fft_2.data();
	FloatComplex *fft_3 = mem_fft_3.data();
	float *tff_1 = mem_tff_1.data();
	float *tff_2 = mem_tff_2.data();
	float *tff_3 = mem_tff_3.data();
	FloatComplex *deriv_x = mem_deriv_x.data();
	FloatComplex *deriv_y = mem_deriv_y.data();
	FloatComplex *deriv_z = mem_deriv_z.data();
	float *diffusion_p1_noh = mem_diffusion_p1.data();
	
	float* p_term = mem_p_term.data();
	float* q_term = mem_q_term.data();
	

	printMemoryRecords();
//////////////////////////////////////////////////////////////////

	//spočítání k.^2
	if(homogenous){
		fprintf(stderr, "\n%sk2\n", bc.ENDC);
		for (int z = 0; z < FullDim[kd.z]; z++)
		{
			for (int y = 0; y < FullDim[kd.y]; y++)
			{
				for (int x = 0; x < FullDim[kd.x];  x++)
				{
					int i = get_index(x, y, z, FullDim);
					k[i] = k[i] * k[i];
				}
			}
		}
	}
	printf("k2\n");

	fprintf(stdout, "------------------------------------------------------------------\n");

	printf("Výpočet %p %p %p %p %p %p\n", T, k, kappa, tmpFft, tmpTff, d_term);


	if(homogenous){
		k = full2reducedR(&mem_k);
		kappa = full2reducedR(&mem_kappa);
		if(use_perfusion){
			q_term = homo2nohomo(&mem_q_term);
		}
	} else {
		deriv_x = full2reducedC(&mem_deriv_x);
		deriv_y = full2reducedC(&mem_deriv_y);
		if(dimension_count == 3){
			deriv_z = full2reducedC(&mem_deriv_z);
		}
		diffusion_p1_noh = homo2nohomo(&mem_diffusion_p1);
		q_term = homo2nohomo(&mem_q_term);
		if(use_perfusion){
			kappa = full2reducedR(&mem_kappa);
			homo2nohomo(&mem_perfusion_coeff);
			homo2nohomo(&mem_blood_ambient_temperature);
		}
	}

	use_q_term = !!(mem_q_term.size() - 1) + (mem_q_term[0] != 0);
	homo_diffusion_p1 = !(mem_diffusion_p1.size() - 1);
	

	printMemoryRecords();
	printf(bc.YELLOW);
	printf("Nastavení:\n");
	printf("\tdim count: %d\n", dimension_count);
	printf("\tuse_perfusion: %d\n", use_perfusion);
	printf("\thomogenous: %d\n", homogenous);
	printf("\tq_term %d\n", use_q_term);
	printf("\tTIME: %d\n", TIME);
	printf("\tdt: %e\n", dt);
	printf("\tDim X: %d\n", FullDim[kd.x]);
	printf("\tDim Y: %d\n", FullDim[kd.y]);
	printf("\tDim Z: %d\n", FullDim[kd.z]);
	printf("\tdiffusion_p1 %e  homo %d  size %d\n", diffusion_p1, homo_diffusion_p1, mem_diffusion_p1.size());
	printf("\tdiffusion_p2 %e  size %d\n", diffusion_p2, mem_diffusion_p2.size());
	if(use_perfusion){
		printf("\tperfusion_coeff %e  size %d\n", mem_perfusion_coeff[0], mem_perfusion_coeff.size());
		printf("\tblood_ambient_temperature %e  size %d\n", mem_blood_ambient_temperature[0], mem_blood_ambient_temperature.size());
	}
	printf("%s\n", bc.ENDC);
	
	
	printMemoryUsage();
	
	///////////////////////////////////////////////////////////////////
	copyAllToGPU();
	
	Timer Tim;

	printf("%s\nSimulation Begin\n", bc.CURSORH);
	Tim.setInterval(sim_info, 100);

	//Hlavní výpočetní smyčka
	for(sim_time = 0; sim_time < TIME; ++sim_time){

		timerStart();

		if(homogenous){
			#pragma acc data present(T, k, kappa, tmpFft, tmpTff, d_term, ReducedDim, FullDim)
			{
				#include "homo.hpp"
			}
		}
		else {
			#pragma acc data present(T, tmpFft, d_term, ReducedDim, FullDim\
			,deriv_x, deriv_y, fft_1, fft_2, fft_3, tff_1, tff_2, tff_3, diffusion_p1_noh)
			{
				#include "nohomo.hpp"
			}
		}
		
		timerStop();
		step_time = timer_us;
		diameter_step_size += timer_us;
		if(sim_time == 100){
			fprintf(stderr, "\nTime for step %ldus diameter %lfus  width: %d\n", step_time, diameter_step_size / sim_time, FullDim[0]);
		}
	
	}

	Tim.stop();
	printf("%s", bc.CURSORS);

	sim_info();
	printf("\n\n");

	copyAllToHost();
	

	printf(bc.ENDC);
	return 0;
}





