/**
 * @file      container.cpp
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Soubor implementace kontejneru matic
 *
 * @version   heatsim 1.0
 *
 * @date      19.5.2021
 */

#include "container.hpp"
#include <vector>
#include <stdio.h>
#include "zaklad/zaklad.hpp"

using namespace std;
using FloatComplex = std::complex<float>;

static vector<GpuMem<float>*> database;

//přidává záznam do database
void addMemoryRecord(GpuMem<float>* M){
	database.push_back(M);
}

void addMemoryRecord(GpuMem<std::complex<float>>* M){
	database.push_back((GpuMem<float>*)M);
}

void addMemoryRecord(GpuMem<int>* M){
	database.push_back((GpuMem<float>*)M);
}

//přepíše hledaný záznam na NULL
void delMemoryRecord(void* M){
	for(auto &a : database){
		if(a == M){
			a = NULL;
		}
	}
}

//vytiskne info o všech záznamech
void printMemoryRecords(void){
	for(auto *a : database){
		if(a)
			printf("%sMemoryRecord:%s %p\tsize %d\tdata %p\tName: %s\n", 
				bc.RED, bc.ENDC, a, a->size(), a->data(), a->name().c_str());
		else
			printf("%sMemoryRecord:%s DELETED\n", bc.RED, bc.ENDC);
	}
}

//vypočítá spotřebovanout paměť
void printMemoryUsage(){
	unsigned long cpu = 0;
	unsigned long gpu = 0;
	for(auto *a : database){
		if(a){
			cpu += a->size() * a->ofsize();
			if(a->useGPU()){
				gpu += a->size() * a->ofsize();
			}
		}
	}
	float cpuf = cpu / 1000000.0f;
	float gpuf = gpu / 1000000.0f;
	float cpuff = cpu / 1024.0 / 1024.0;
	float gpuff = cpu / 1024.0 / 1024.0;

#if defined (GPU_ACC) || defined(GPU_CUDA)
	printf("%sMemoryUsed:%s RAM: %f MiB  GPU: %f MiB\n", bc.RED, bc.ENDC, cpuf, gpuf);
	printf("%sMemoryUsed:%s RAM: %f MB   GPU: %f MB\n\n", bc.RED, bc.ENDC, cpuff, gpuff);
#else
	printf("%sMemoryUsed:%s RAM: %f MiB\n", bc.RED, bc.ENDC, cpuf);
	printf("%sMemoryUsed:%s RAM: %f MB\n\n", bc.RED, bc.ENDC, cpuff);
#endif
}

//Volání metod pro všechny záznamy
void createAllOnGPU(void){
	for(auto *a : database){
		if(a)
			a->createOnDev();
	}
}

void copyAllToGPU(void){
	for(auto *a : database){
		if(a)
			a->cpyTD();
	}
}

void copyAllToHost(void){
	for(auto *a : database){
		if(a)
			a->cpyTH();
	}
}

void deleteAllOnGPU(void){
	for(auto *a : database){
		if(a)
			a->deleteOnDev();
	}
}




