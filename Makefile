#nvidia-smi


ACC_NUM_CORES = $(shell nproc)

export ACC_NUM_CORES
VARIABLE=$(shell echo $$ACC_NUM_CORES)

CXX = g++
#PGC = ~/pgc/linux86-64-nollvm/19.4/bin/pgcc
#PGCP = ~/pgc/linux86-64-nollvm/19.4/bin/pgc++
PGC = pgcc
PGCP = pgc++

NVCP = nvc++
NVCC = nvcc

#COMMAND = -o tester tester.cpp mat_add.cpp mat_jacobi.cpp mat_memcpy.cpp mat_mul.cpp mat_trans.cpp templates.cpp
COMMAND = -o heatsim *.cpp zaklad/*.cpp

#PGCFLAGS = -fast -Minfo=accel -Mvect=levels:5 #--display_error_number --diag_error 144
#PGCFLAGS = -O3 -Mvect=levels:5 -Minfo=accel #--display_error_number --diag_error 144
#PGCFLAGS = -O3 -Mvect=levels:5 #--display_error_number --diag_error 144
#PGCFLAGS = -fast -Minfo=accel,loop,par,vect -Mvect=levels:5 #--display_error_number --diag_error 144
#PGCFLAGS = -fast -Minfo=all #--display_error_number --diag_error 144
PGCFLAGS = -Minfo=accel -Mvect=levels:5 #-ta=tesla:pinned -Mcuda
#CFLAGS = -g
CFLAGS = -O3
CPPFLAGS = -std=c++17 #-Wno-write-strings $(DARGS)
GCCFLAGS = -march=native -mtune=native -fopt-info-vec
LDFLAGS = -lfftw3f -lhdf5 -lhdf5_hl -lm -lz -lcufft -lpthread -lhdf5_cpp

#--generate-code arch=compute_30,code=sm_30
CUDA_ARCH = --generate-code arch=compute_35,code=sm_35 \
            --generate-code arch=compute_37,code=sm_37 \
            --generate-code arch=compute_50,code=sm_50 \
            --generate-code arch=compute_52,code=sm_52 \
            --generate-code arch=compute_53,code=sm_53 \
            --generate-code arch=compute_60,code=sm_60 \
            --generate-code arch=compute_61,code=sm_61 \
            --generate-code arch=compute_62,code=sm_62 \
            --generate-code arch=compute_70,code=sm_70 \
            --generate-code arch=compute_72,code=sm_72 \
            --generate-code arch=compute_75,code=sm_75

CUDA_FLAGS = --device-c --restrict -DGPU_CUDA
TARGET       = main
DEPENDENCIES = main.o templates.o container.o zaklad/zaklad.o gpu_cuda.o helpers.o storage.o


all: gcc
	#make gcc -j$(ACC_NUM_CORES)

folders: clean
	[ -d data ] || mkdir data
	[ -d data/data_input ] || mkdir data/data_input
	[ -d data/data_output ] || mkdir data/data_output
	[ -d data/data_controll ] || mkdir data/data_controll

gcc: folders
	$(CXX) -DGCC -fpermissive $(CFLAGS) $(CPPFLAGS) $(GCCFLAGS) $(COMMAND) $(LDFLAGS)

gccgpu: folders
	$(CXX) -DGCC -fpermissive -fopenacc $(CFLAGS) $(CPPFLAGS) $(GCCFLAGS) $(COMMAND) $(LDFLAGS)

cpu: folders
	$(PGCP) -DCPU_ACC -ta=host $(PGCFLAGS) $(CFLAGS) $(CPPFLAGS) $(COMMAND) $(LDFLAGS)

multi: folders
	$(PGCP) -DCPU_ACC -ta=multicore $(PGCFLAGS) $(CFLAGS) $(CPPFLAGS) $(COMMAND) $(LDFLAGS)

gpu: folders
	$(PGCP) -DGPU_ACC -ta=tesla:pinned,zeroinit -Mcuda $(PGCFLAGS) $(CFLAGS) $(CPPFLAGS) $(COMMAND) $(LDFLAGS)

nvc: folders
	$(NVCP) -DGPU_ACC -acc=gpu -gpu=pinned,zeroinit -Mcuda $(PGCFLAGS) $(CFLAGS) $(CPPFLAGS) $(COMMAND) $(LDFLAGS)

#nvcp: folders
#	$(NVCP) -DGPU_ACC -acc=gpu -gpu=pinned,zeroinit $(PGCFLAGS) $(CFLAGS) $(CPPFLAGS) $(COMMAND) $(LDFLAGS)

debug: folders
	$(CXX) -DGCC -fpermissive -g3 $(CPPFLAGS) $(COMMAND) $(LDFLAGS)

debuggpu: folders
	$(PGCP) -DGPU_ACC -ta=tesla:pinned,time $(PGCFLAGS) -g $(CPPFLAGS) $(COMMAND) $(LDFLAGS)

cuda: folders $(DEPENDENCIES)
	$(NVCC) $(LDFLAGS) $(DEPENDENCIES) $(LDLIBS) -o heatsim

# Compile CPU units
%.o: %.cpp
	$(NVCC) $(CFLAGS) $(CUDA_FLAGS) -o $@ -c $<
	#$(CXX) -DGPU_CUDA  $(CFLAGS) $(CPPFLAGS) $(GCCFLAGS) -o $@ -c $<

# Compile CUDA units
%.o: %.cu
	$(NVCC) $(CFLAGS) $(CUDA_FLAGS) $(CUDA_ARCH) -o $@ -c $<

run:
	setarch $(uname -m) -R ./heatsim ${ARGS}

matlab1:
	(cd k-Wave && matlab -nosplash -nodesktop -r "run('example_diff_homogeneous_medium_diffusion.m');")

matlab1e:
	(cd k-Wave && matlab -nodisplay -nosplash -nodesktop -r "run('example_diff_homogeneous_medium_diffusion.m');exit;")

matlab2:
	(cd k-Wave && matlab -nosplash -nodesktop -r "run('example_diff_homogeneous_medium_source.m');")

matlab2e:
	(cd k-Wave && matlab -nodisplay -nosplash -nodesktop -r "run('example_diff_homogeneous_medium_source.m');exit;")

clean:
	rm -f *.o
	rm -f zaklad/*.o
	rm -f heatsim

mrproper: clean
	rm -rf data

info:
	@echo cores $(ACC_NUM_CORES)
	@echo CXX $(CXX)
	@echo PGC++ $(PGCP)
	@echo nvc $(NVCP)

#--diag_suppress
#--diag_warning
##test
