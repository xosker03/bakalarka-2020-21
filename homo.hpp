/**
 * @file      homo.hpp
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Rozšíření souboru main.cpp
 *
 * @version   heatsim 1.0
 *
 * @date      19.5.2021
 */

#ifndef GPU_CUDA

int R = get_size(FullDim);
int C = get_size(ReducedDim);

//p_term = - obj.perfusion_coeff .* obj.IT( kappa .* obj.FT(obj.T - obj.blood_ambient_temperature) );
if(use_perfusion)
	a_perfusion(R, C, p_term, T, kappa,
		tmpFft, mem_perfusion_coeff[0], mem_blood_ambient_temperature[0], idivider);

//obj.FT(obj.T)
fftR2C(T, tmpFft);		

//-obj.k.^2 .* kappa .* obj.FT(obj.T)
a_after_fft(C, tmpFft, k, kappa);

//obj.IT( -obj.k.^2 .* kappa .* obj.FT(obj.T) )
fftC2R(tmpFft, tmpTff);
a_after_tff(R, tmpTff, idivider);
		
//d_term = obj.diffusion_p1 .* obj.diffusion_p2 .* obj.IT( -obj.k.^2 .* kappa .* obj.FT(obj.T) )
a_d_term(R, d_term, tmpTff, diffusion_p1, diffusion_p2);


//obj.T = obj.T + dt .* ( d_term + p_term + q_term );
if(use_perfusion | use_q_term){
	a_T(R, T, d_term, p_term, q_term, dt);
} else {
	a_T(R, T, d_term, dt);
}

//obj.cem43 = obj.cem43 + dt ./ 60 .* ( 0.5 .* (obj.T >= 43) + 0.25 .* (obj.T >= 37 & obj.T < 43 ) ).^(43 - obj.T);
a_cem43(R, cem43, T, dt);


#else


int R = get_size(FullDim);
int C = get_size(ReducedDim);

//p_term = - obj.perfusion_coeff .* obj.IT( kappa .* obj.FT(obj.T - obj.blood_ambient_temperature) );
if(use_perfusion)
	c_perfusion(R, C, mem_p_term.g_data(), mem_T.g_data(), mem_kappa.g_data(),
			mem_tmpFft.g_data(), mem_perfusion_coeff[0], mem_blood_ambient_temperature[0], idivider);

//obj.FT(obj.T)
fftR2C(mem_T.g_data(), mem_tmpFft.g_data());

//-obj.k.^2 .* kappa .* obj.FT(obj.T)
c_after_fft(mem_tmpFft.g_data(), mem_k.g_data(), mem_kappa.g_data(), C);


//obj.IT( -obj.k.^2 .* kappa .* obj.FT(obj.T) )
fftC2R(mem_tmpFft.g_data(), mem_tmpTff.g_data());
c_after_tff(mem_tmpTff.g_data(), R, idivider);


//d_term = obj.diffusion_p1 .* obj.diffusion_p2 .* obj.IT( -obj.k.^2 .* kappa .* obj.FT(obj.T) )
c_d_term(mem_d_term.g_data(), mem_tmpTff.g_data(), R, diffusion_p1, diffusion_p2);
		

//obj.T = obj.T + dt .* ( d_term + p_term + q_term );
if(use_perfusion | use_q_term){
	c_T(mem_T.g_data(), mem_d_term.g_data(), mem_p_term.g_data(), mem_q_term.g_data(), R, dt);
} else {
	c_T(mem_T.g_data(), mem_d_term.g_data(), R, dt);
}

//obj.cem43 = obj.cem43 + dt ./ 60 .* ( 0.5 .* (obj.T >= 43) + 0.25 .* (obj.T >= 37 & obj.T < 43 ) ).^(43 - obj.T);
c_cem43(R, mem_cem43.g_data(), mem_T.g_data(), dt);
		

#endif


