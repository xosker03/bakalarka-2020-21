/**
 * @file      helpers.hpp
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Hlavičkový soubor helpers.cpp
 *
 * @version   heatsim 1.0
 *
 * @date      19.5.2021
 */

#ifndef _HELPERS_HPP_
#define _HELPERS_HPP_

#include <complex>

using FloatComplex = std::complex<float>;

void print_mat(float *m, char *name, char *color, int file = 0, bool full = true);
void print_mat(FloatComplex *m, char *name, char *color, int file = 0, bool full = false);



#endif
