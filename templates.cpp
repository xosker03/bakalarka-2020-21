/**
 * @file      templates.cpp
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Soubor obsahující implementaci třídy GpuMem
 *
 * @version   heatsim 1.0
 *
 * @date      19.5.2021
 */

#include "templates.hpp"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "container.hpp"
#include "assert.h"

#include "zaklad/zaklad.hpp"
#include "storage.hpp"

#ifdef GPU_CUDA
	#include <cuda_runtime_api.h>
	#include <cuda.h>
#endif

using FloatComplex = std::complex<float>;

template<class TYPE> GpuMem<TYPE>::GpuMem(uint64_t size, 
		std::string name, bool use_gpu, bool load, bool store, bool alloc)
{
	__data = NULL;
	__use_gpu = use_gpu;
	__load = load & alloc;
	__store = store;

	if(alloc){
		if(__load){
			__size = loadhdf5(&__data, name);
		} else {
			__data = (TYPE *) calloc(size, sizeof(TYPE));
			__size = size;
		}
	} else {
		__data = NULL;
		__size = 0;
		__load = false;
		__store = false;
		__use_gpu = false;
	}
    __onDevice = false;
	__name = name;
	addMemoryRecord(this);
	__data_sizeof = sizeof(TYPE);

}

template<class TYPE> GpuMem<TYPE>::GpuMem(uint64_t size, std::string name) 
	: GpuMem(size, name, false, false, false, true)
{
}

template<class TYPE> GpuMem<TYPE>::GpuMem(uint64_t size) : GpuMem(size, "")
{

}


template<class TYPE> GpuMem<TYPE>::GpuMem(GpuMem<TYPE> &other)
{
    if(other.__onDevice == true)
    {
        other.cpyTH();
    }
    __size = other.__size;
    __onDevice = false;
    __data = (TYPE *) calloc(__size, sizeof(TYPE));
    if(__data == NULL)
    {
        throw "GpuMem::GpuMem nedostatek pameti";
    }
    for(uint64_t i = 0; i < __size; ++i)
    {
        __data[i] = other.__data[i];
    }
}

template<class TYPE> GpuMem<TYPE>::~GpuMem()
{
	delMemoryRecord((void *) this);
	if(__store){
		storehdf5(__data, __name);
	}

    if(__data != NULL)
    {
        if(this->__onDevice)
        {
            this->deleteOnDev();
        }
        free(this->__data);
    }
}

template<class TYPE> void GpuMem<TYPE>::dangerous_override(TYPE * nd, int ns)
{
	printf("%s", bc.CRITIC);
	printf("WARNING Matrix %s Override %p --> %p, size %d --> %d", __name.c_str(), __data, nd, __size, ns);
	printf("%s\n", bc.ENDC);
	assert(!__onDevice && "__onDevice");
	free(__data);
	__data = nd;
	__size = ns;
}

template<class TYPE> TYPE * GpuMem<TYPE>::data()
{
    return this->__data;
}

template<class TYPE> TYPE * GpuMem<TYPE>::g_data()
{
    return this->__g_data;
}

template<class TYPE> uint64_t GpuMem<TYPE>::size()
{
    return this->__size;
}
template<class TYPE> uint64_t GpuMem<TYPE>::ofsize()
{
    return this->__data_sizeof;
}

template<class TYPE> std::string GpuMem<TYPE>::name()
{
    return this->__name;
}

template<class TYPE> bool GpuMem<TYPE>::onDev()
{
    return this->__onDevice;
}
template<class TYPE> bool GpuMem<TYPE>::useGPU()
{
    return this->__use_gpu;
}

template<class TYPE> void GpuMem<TYPE>::createOnDev()
{
	if(__use_gpu){
#ifdef GPU_ACC
	printf("createOnGPU %p %p %d %d %s\n", this, __data, __size, __data_sizeof, __name.c_str());

    #pragma acc enter data create(this)
    #pragma acc update device(this)
	if(__data_sizeof == sizeof(float)){
		#pragma acc enter data create(__data[0:__size])
	} else {
		FloatComplex *temp = (FloatComplex *) __data;
		#pragma acc enter data create(temp[0:__size])
	}
#endif
#ifdef GPU_CUDA
    cudaMalloc((void **)&__g_data, __size * __data_sizeof);
	printf("CUDA createOnGPU %p %p %d %d %s\n", __data, __g_data, __size, __data_sizeof, __name.c_str());
#endif
    this->__onDevice = true;
	}
}

template<class TYPE> void GpuMem<TYPE>::deleteOnDev()
{
	if(__use_gpu){
#ifdef GPU_ACC
	printf("deleteOnGPU %p %p %d\n", this, __data, __size);
	if(__data_sizeof == sizeof(float)){
		#pragma acc exit data delete(__data[0:__size])
	} else {
		FloatComplex *temp = (FloatComplex *) __data;
		#pragma acc exit data delete(temp[0:__size])
	}
    #pragma acc exit data delete(this)
#endif
#ifdef GPU_CUDA
	printf("CUDA deleteOnGPU %p %p %d\n", __data, __g_data, __size);
	cudaFree(__g_data);
#endif
    this->__onDevice = false;
	}
}


template<class TYPE> void GpuMem<TYPE>::cpyTD()
{
    this->cpyTD(0, this->__size);
}
template<class TYPE> void GpuMem<TYPE>::cpyTD(uint64_t from, uint64_t count)
{
	if(__use_gpu){
#ifdef GPU_ACC
	printf("copyToGPU %p %p %p %d\n", this, __data, __size);
    if(this->__onDevice == false)
    {
        this->createOnDev();
    }
    #pragma acc update device(this)
	if(__data_sizeof == sizeof(float)){
		#pragma acc update device(__data[from:count])
	} else {
		FloatComplex *temp = (FloatComplex *) __data;
		#pragma acc update device(temp[from:count])
	}
#endif
#ifdef GPU_CUDA
	printf("CUDA copyToGPU %p %p %d\n", __data, __g_data, __size);
    if(this->__onDevice == false)
    {
        this->createOnDev();
    }
	cudaMemcpy(__g_data, __data, __size * __data_sizeof, cudaMemcpyHostToDevice);   
#endif
	}
}


template<class TYPE> void GpuMem<TYPE>::cpyTH()
{
    this->cpyTH(0, this->__size);
}
template<class TYPE> void GpuMem<TYPE>::cpyTH(uint64_t from, uint64_t count)
{
	if(__use_gpu){
#ifdef GPU_ACC
	printf("copyToHost %p %p %d\n", this, __data, __size);
    if(this->__onDevice == false)
    {
        throw "GpuMem::cpyTH neexistuje na deevice";
    }
	if(__data_sizeof == sizeof(float)){
		#pragma acc update self(__data[from:count])
	} else {
		FloatComplex *temp = (FloatComplex *) __data;
		#pragma acc update self(temp[from:count])
	}
#endif
#ifdef GPU_CUDA
	printf("CUDA copyToHost %p %p %d\n", __data, __g_data, __size);
    if(this->__onDevice == false)
    {
        throw "GpuMem::cpyTH neexistuje na deevice";
    }
	cudaMemcpy(__data, __g_data, __size * __data_sizeof, cudaMemcpyDeviceToHost);
#endif
	}
}


template<class TYPE> TYPE & GpuMem<TYPE>::operator [] (int i)
{
    return this->__data[i];
}

template<class TYPE> bool GpuMem<TYPE>::operator == (GpuMem<TYPE> &other)
{
    bool result = true;

    if(__size != other.__size)
    {
        return false;
    }

    for(uint64_t i = 0; i < __size; ++i)
    {
        if(__data[i] == other.__data[i])
        {
        }
        else
        {
            result = false;
            break;
        }
    }
    return result;
}

template<class TYPE> bool GpuMem<TYPE>::operator != (GpuMem<TYPE> &other)
{
    return ! (*this == other);
}



template class GpuMem<float>;
template class GpuMem<std::complex<float>>;
template class GpuMem<int>;
