/**
 * @file      templates.hpp
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Hlavičkový soubor třídy GpuMem
 *
 * @version   heatsim 1.0
 *
 * @date      19.5.2021
 */

#ifndef _TEMPLATES_HPP_
#define _TEMPLATES_HPP_

#include <stdint.h>
#include <complex>
#include <string>

template<class TYPE>
class GpuMem
{
public:
    GpuMem(uint64_t size, std::string, bool use_gpu, bool load, bool store, bool alloc);
    GpuMem(uint64_t size, std::string);
    GpuMem(uint64_t size);
    GpuMem(GpuMem<TYPE> &other);
    virtual ~GpuMem();

	void dangerous_override(TYPE * nd, int ns);

    TYPE * data();
	TYPE * g_data();
    uint64_t size();
    uint64_t ofsize();
	std::string name();
    bool onDev();
    bool useGPU();
	//create / delete on device
    void createOnDev();
    void deleteOnDev();
    //copy to device
    void cpyTD();
    void cpyTD(uint64_t from, uint64_t count);
    //copy to host
    void cpyTH();
    void cpyTH(uint64_t from, uint64_t count);

    TYPE & operator [] (int i);
    bool operator == (GpuMem<TYPE> &other);
    bool operator != (GpuMem<TYPE> &other);

private:
    TYPE *__data;
    TYPE *__g_data;
    uint64_t __size;
	std::string __name;
    bool __onDevice;
	int __data_sizeof;
	bool __use_gpu;
	bool __load;
	bool __store;
};


#endif
