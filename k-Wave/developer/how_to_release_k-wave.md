# HOW TO RELEASE K-WAVE

_Last updated 18th December 2018_ 

## 1. Overview

These notes outline the procedures and tasks needed to prepare k-Wave for release.

## 2. Final MATLAB Code Updates

* Create a testing branch for the release.

        git checkout -b test_release_V1.2
    
* Check that any examples with multiple options have the correct default option set. In general, these will be correct, but might accidently be incorrect if the examples have been updated.
  * Comparison Of Modelling Functions Example (`example_number = 1;`)
  * Diffraction Through A Slit Example (`example_number = 1;` and `scale = 1;)` and `barrier_scale = 20;`)
  * Attenuation Compensation Using Time Variant Filtering Example (`USE_PARALLEL_COMPUTING_TOOLBOX = false;`)
  * Simulating Ultrasound Beam Patterns Example (`DATA_CAST = 'single';` and `MASK_PLANE = 'xy';` and `USE_STATISTICS = true;`)
  * Simulating B-mode Ultrasound Images Example (`DATA_CAST = 'single';` and `RUN_SIMULATION = true;`)
  * Simulating B-mode Images Using A Phased Array Example (`DATA_CAST = 'single';` and `RUN_SIMULATION = true;`)
  * Controlling The Absorbing Boundary Layer Example (`example_number = 1;`)
  * Filtering A Delta Function Input Signal Example - Part 3 (`example_number = 1;`)
  * Modelling Power Law Absorption Example (`example_number = 1;`)
  * Optimising k-Wave Performance Example (`example_number = 1;` and `scale = 1;`)
  * Running C++ Simulations Example (`example_number = 1;`)
  * Saving Input Files In Parts Example (`example_number = 1;`)
  * Shear Waves And Critical Angle Reflection Example (`scale = 1;`)

- Search all files to check data casting is not set to `'gpuArray-single'` anywhere.
- Check that `Functions — By Category` and `Functions — Alphabetical List` are up to date.
- Update `Contents.m` and `k-Wave.m` based on `Functions — By Category`.
- Update release date in:
  * `/helpfiles/k-wave_release_notes.html`
  * `Contents.m`
  * `k-wave.m`
  * `../readme.txt`
- Update version number in:
  * `/helpfiles/k-wave_release_notes.html`
  * `Contents.m`
  * `k-wave.m`
  * `info.xml`
  * `getkWaveVersion.m`
  * `../readme.txt`
- Update copyright year range in `/helpfiles/k-wave_license.html`.
* Update `.gitattributes` file to make sure all development and testing files, and binaries are excluded from the release. All `.gitignore` files should be added to this file.
* Build release candidates using:

        git archive development –o k-wave-toolbox-version-1.2-RC1.zip
    
* Idiot check release candidate has nothing in the binaries folder, no videos in the examples folder, no temporary files, no developer or testing files, no .gitignore files, etc.

## 3. Testing
* Create testing issue in GitLab with test matrix (see e.g., <a href="https://bug.medphys.ucl.ac.uk:10080/bug/k-wave-matlab/issues/106">#106</a>).
* Update `testing/regression/generateRegressionData` to make sure all examples in the previous release are covered.
* Run `testing/regression/generateRegressionData` using the previous release to generate new regression test data (this shouldn't be commited to the repository).
* Run `testing/regression/runRegressionTests.m` across Windows, Linux, and Mac.
* Run `testing/unit/runUnitTests.m` across Windows, Linux, and Mac.
* Run `testing/kWaveTester/kwt_matlab_run_all_tests.m` to check for run time MATLAB errors.
* Run `testing/kWaveTester/kwt_run_omp_comparison_tests.m` to test C++ code output against MATLAB on both Linux and Windows.
* Run `testing/kWaveTester/kwt_run_cuda_comparison_tests.m` to test CUDA code output against MATLAB on both Linux and Windows.

## 4. Release
* Merge the development branch with the master branch using the `Merge To Master Template` on GitLab.
* Switch to the master branch, pull from the repository, and tag the release version using (change the release version number as appropriate):

        git tag -a v1.2 -m “k-Wave Release v1.2”
        git push --tags

* Build the release version using (change the release version number as appropriate):

        git archive master -o k-wave-toolbox-version-1.2.zip

* Copy the file to the `/securefiles` folder of the k-Wave website.
* Update `getFile.php` to list the link to the new file, giving it a new unique file ID and meta name. The meta name is added to the user profile along with the date when downloaded.
* Move information about the previous release to `downloadprevious.php`.
* Update `download.php` to link to the new versions using the file ID, e.g., `<a href="http://www.k-wave.org/getfile.php?id=8">`.
* Update `forum/bb-admin/get-download-stats.php` with the new meta names to track downloads.
* Update `releasenotes.php`.
* Update `index.php` to add a note to front page news feed. 

## 5. Generating Online MATLAB Documentation

* To update the online MATLAB documentation, copy everything from the `help` folder in k-Wave (including the `images` subfolder), to the `documentation` folder on the website (archive the old version).
* From the previous version of the documentation, copy the following files into the new documentation folder:
  * `analyticstracking.php`
  * `documentationfooter.php`
  * `documentationheader.php`
  * `index.php`
  * `style.css`
  * `images/vline.png`
  * `images/lastnode.png`
  * `images/node.png`
* Update the table of contents in `documentationfooter.php` (use `helptoc.xml` and `k-wave_examples.html` for reference).
* Rename the `.html` file extensions to `.php` using a terminal window. Change directories into the relevant folder and then run

        for file in *.html; do mv "$file" "${file/.html/.php}"; done

* Using multi-file search in TextWrangler (or similar), replace `.html` with `.php`
* Using multi-file search in TextWrangler (or similar), replace 

        </body></html>

  with 

        <?php include("documentationfooter.php"); ?>

* Using multi-file _grep_ search in TextWrangler (or similar), replace 

        <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4\.01 Transitional//EN" "http://www\.w3\.org/TR/1999/REC-html401-19991224/loose\.dtd">
        <html lang="en">
        <head>
	        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	        <title>.*</title>
	        <link rel="stylesheet" href="kwavehelpstyle\.css" type="text/css">
        </head>
                   
        <body>

  and

        <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4\.01 Transitional//EN" "http://www\.w3\.org/TR/1999/REC-html401-19991224/loose\.dtd">
        <html lang="en">
        <head>
        	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        	<title>.*</title>
        	<link rel="stylesheet" href="kwavehelpstyle\.css" type="text/css">
        	<meta name="description" content=".*">
        </head>
        
        <body>

  with

        <?php $pagetitle=" - k-Wave MATLAB Toolbox"; include("documentationheader.php"); ?>

* Using multi-file _grep_ search in TextWrangler (or similar), delete the following text (replace with blank)

        <p>
            <ul>
                <li><a href="matlab:edit\(\[getkWavePath\('examples'\) '.*']\);" target="_top">Open the file in the MATLAB Editor</a></li>
                <li><a href="matlab:run\(\[getkWavePath\('examples'\) '.*'\]\);" target="_top">Run the file in MATLAB</a></li>
            </ul>
        </p>

* Using multi-file _grep_ search in TextWrangler (or similar), replace `<a href="matlab: doc .*">` with `<a>`
* In k-wave.php, replace

        <html>
        <head>
            <title>k-Wave Toolbox</title>
            <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
            <link rel="stylesheet" href="kwavehelpstyle.css" type="text/css">
            <style type="text/css">
                ul {
                list-style-type: disc;
                list-style-image: url(bullet.gif);
                }
            </style>
        </head>
        
        <body>

  with

        <?php $pagetitle="MATLAB Documentation - k-Wave MATLAB Toolbox"; include("documentationheader.php"); ?>

* In `k-wave_getting_started.php`, replace

        click <a href="matlab:edit([getkWavePath('examples') 'example_ivp_homogeneous_medium.m']);" target="_top">open the file</a>

  with

        click <em>open the file</em>`

* Archive the old documentation, and copy the new documentation folder to mason `web/kwave/documentation`.

## 6. Emailing Users

* Generate a list of current registered users who have elected to receive updates using <a href="http://www.k-wave.org/forum/bb-admin/get-list-of-user-emails.php">http://www.k-wave.org/forum/bb-admin/get-list-of-user-emails.php</a> (this requires admin login to the k-Wave website). For data security, do not save this list anywhere.
* Login to <a href="http://123-reg.co.uk">http://123-reg.co.uk</a>.
* Navigate to Control Panel -> Your Hosting: k-wave.org [Manage] -> Mailing Lists.
* Create a new mailing list, e.g., kwave1.
* Select "Edit List".
* In blocks of < 2000, copy the user emails into the box, and select subscribe. Ignore the message if any email addresses are reported as not valid.
* Select the home button, then click on "Mailing Lists"
* Select "Email Subscribers"
  * Send From Email Address: updates@k-wave.org
  * Email Subject: New k-Wave Toolbox Release (V1.2)
  * Content Type: Plain text
  * Typical Email Text:

> Dear Colleagues,
> ​        
> We are pleased to announce the release of k-Wave version 1.2. The toolbox files and source codes can be downloaded from http://www.k-wave.org/download.php. This release includes new codes for the time-domain solution of the diffusion equation or Pennes' bioheat equation. Additional release notes can be found at http://www.k-wave.org/releasenotes.php. If you have any feedback or comments, or wish to report bugs, obtain help, or request new features, please visit the k-Wave forum at http://www.k-wave.org/forum.
> ​       
> Kind regards,
> 
> Bradley Treeby, Ben Cox, and Jiri Jaros.
> 
> \---
> 
> If you no longer wish to receive updates about k-Wave releases, please update your preferences at http://www.k-wave.org/forum/bb-login.php

* Repeat as many times as needed to email all users (add your email to the end for confirmation).
* Delete all mailing lists for data protection.