/**
 * @file      container.hpp
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Hlavičkový soubor kontejneru matic
 *
 * @version   heatsim 1.0
 *
 * @date      19.5.2021
 */

#ifndef _CONTAINER_HPP_
#define _CONTAINER_HPP_

#include <complex>
#include "templates.hpp"

void addMemoryRecord(GpuMem<float>* M);
void addMemoryRecord(GpuMem<std::complex<float>>* M);
void addMemoryRecord(GpuMem<int>* M);
void delMemoryRecord(void* M);
void printMemoryRecords(void);
void printMemoryUsage();
void createAllOnGPU(void);
void copyAllToGPU(void);
void copyAllToHost(void);
void deleteAllOnGPU(void);


#endif
