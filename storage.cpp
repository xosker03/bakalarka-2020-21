/**
 * @file      storage.cpp
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Soubor obsahující implementaci pro práci s hdf5 soubory
 *
 * @version   heatsim 1.0
 *
 * @date      19.5.2021
 */

#include "storage.hpp"

#include <stdio.h>
#include <stdint.h>
#include <iostream>
#include <hdf5.h>
#include <H5Cpp.h>
#include "main.hpp"
#include "zaklad/zaklad.hpp"

using namespace std;
using namespace H5;

//načítá matici z hdf5 souboru
int loadhdf5(int **data, string name){
	int new_size = 0;
	int *new_data = NULL;

	string file_path = PATH_INPUT + "/" + name + ".h5";
	cout << bc.GREEN << "H5 load:" << bc.ENDC << " otevírám: " << file_path << endl;


	H5File file1( file_path.c_str(), H5F_ACC_RDONLY );
	DataSet dataset1 = file1.openDataSet( name.c_str() );
	DataSpace filespace = dataset1.getSpace();
	int rank = filespace.getSimpleExtentNdims();

	hsize_t *dims = (hsize_t*) calloc(rank, sizeof(hsize_t));
	rank = filespace.getSimpleExtentDims( dims );
	DataSpace mspace1(rank, dims);
    
	cout << "rank " << rank << ", dimensions " <<
          (unsigned long)(dims[0]) << " x " <<
          (unsigned long)(dims[1]) << " x " <<
          (unsigned long)(dims[2]) << endl;

	new_size = 1;
	for(int i = 0; i < rank; ++i)
		new_size *= dims[i];

	new_data = (int *) calloc(new_size, sizeof(int));
	*data = new_data;

	dataset1.read( new_data, PredType::NATIVE_INT, mspace1, filespace );
	

	for(int i = 0; i < new_size; ++i)
		cout << new_data[i] << " ";
	cout << endl;

	free(dims);
	return new_size;
}


int loadhdf5(float **data, string name){
	string file_path = PATH_INPUT + "/" + name + ".h5";
	cout << bc.GREEN << "H5 load:" << bc.ENDC << " otevírám: " << file_path << endl;
	
	int new_size = 0;
	float *new_data = NULL;
	

	H5File file1( file_path.c_str(), H5F_ACC_RDONLY );
	DataSet dataset1 = file1.openDataSet( name.c_str() );
	DataSpace filespace = dataset1.getSpace();
	int rank = filespace.getSimpleExtentNdims();

	hsize_t *dims = (hsize_t*) calloc(rank, sizeof(hsize_t));
	rank = filespace.getSimpleExtentDims( dims );
	DataSpace mspace1(rank, dims);
    
	cout << "rank " << rank << ", dimensions " <<
          (unsigned long)(dims[0]) << " x " <<
          (unsigned long)(dims[1]) << " x " <<
          (unsigned long)(dims[2]) << endl;

	new_size = 1;
	for(int i = 0; i < rank; ++i)
		new_size *= dims[i];

	new_data = (float *) calloc(new_size, sizeof(float));
	*data = new_data;

	dataset1.read( new_data, PredType::NATIVE_FLOAT, mspace1, filespace );
	

	free(dims);
	return new_size;
}


int loadhdf5(FloatComplex **data, string name){
	string file_path = PATH_INPUT + "/" + name + ".h5";
	string file_path_imag = PATH_INPUT + "/" + name + "_imag.h5";
	cout << bc.GREEN <<"H5 load:" << bc.ENDC << " otevírám: " << file_path << "  " << file_path_imag << endl;
	
	int new_size = 0;
	FloatComplex *new_data = NULL;
	float *temp_real = NULL;
	float *temp_imag = NULL;

	H5File file1( file_path.c_str(), H5F_ACC_RDONLY );
	H5File file2( file_path_imag.c_str(), H5F_ACC_RDONLY );
	DataSet dataset1 = file1.openDataSet( name.c_str() );
	DataSet dataset2 = file2.openDataSet( (name + "_imag").c_str() );
	DataSpace filespace = dataset1.getSpace();
	DataSpace filespace2 = dataset2.getSpace();
	int rank = filespace.getSimpleExtentNdims();

	hsize_t *dims = (hsize_t*) calloc(rank, sizeof(hsize_t));
	rank = filespace.getSimpleExtentDims( dims );
	DataSpace mspace1(rank, dims);
	DataSpace mspace2(rank, dims);
    
	cout << "rank " << rank << ", dimensions " <<
          (unsigned long)(dims[0]) << " x " <<
          (unsigned long)(dims[1]) << " x " <<
          (unsigned long)(dims[2]) << endl;

	new_size = 1;
	for(int i = 0; i < rank; ++i)
		new_size *= dims[i];

	new_data = (FloatComplex *) calloc(new_size, sizeof(FloatComplex));
	temp_real = (float *) calloc(new_size, sizeof(float));
	temp_imag = (float *) calloc(new_size, sizeof(float));
	*data = new_data;

	dataset1.read( temp_real, PredType::NATIVE_FLOAT, mspace1, filespace );
	dataset2.read( temp_imag, PredType::NATIVE_FLOAT, mspace2, filespace2 );
	

	for(int i = 0; i < new_size; ++i){
		new_data[i] = FloatComplex(temp_real[i], temp_imag[i]);
	}

	free(dims);
	free(temp_real);
	free(temp_imag);
	return new_size;
}






void storehdf5(int *data, string name){
	string file_path = PATH_OUTPUT + "/" + name + ".h5";
	cout << bc.GREEN << "H5 store:" << bc.ENDC << " otevírám: " << file_path << endl;
	assert(false);
}


//ukládá matici do hdf5 souboru
void storehdf5(float *data, string name){
	string file_path = PATH_OUTPUT + "/" + name + ".h5";
	cout << bc.GREEN << "H5 store:" << bc.ENDC << " otevírám: " << file_path << endl;

	H5File file( file_path.c_str(), H5F_ACC_TRUNC );
    hsize_t     dimsf[3];
    dimsf[0] = FullDim[2];
    dimsf[1] = FullDim[1];
    dimsf[2] = FullDim[0];
	int RANK = 3;
    DataSpace dataspace( RANK, dimsf );
    IntType datatype( PredType::NATIVE_FLOAT );
    datatype.setOrder( H5T_ORDER_LE );
    DataSet dataset = file.createDataSet( name.c_str() , datatype, dataspace );
    dataset.write( data, PredType::NATIVE_FLOAT );	

}


void storehdf5(FloatComplex *data, string name){
	string file_path = PATH_OUTPUT + "/" + name + ".h5";
	string file_path_imag = PATH_OUTPUT + "/" + name + ".h5";
	cout << bc.GREEN << "H5 store: " << bc.ENDC << " otevírám: " << file_path << "  " << file_path_imag << endl;
	assert(false);
}

