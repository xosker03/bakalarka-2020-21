/**
 * @file      gpu_cuda.h
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Hlavičkový soubor pro gpu_cuda.cu
 *
 * @version   heatsim 1.0
 *
 * @date      19.5.2021
 */

#ifndef _GPU_CUDA_HPP_
#define _GPU_CUDA_HPP_

#include <cuda.h>
#include <complex>

using FloatComplex = std::complex<float>;

// HOMOGENOUS

void c_perfusion(int R, int C, float *p_term, float *T, float *kappa,
		FloatComplex *tmpFft, float coef, float temp, float idivider);

void c_after_fft(FloatComplex *tmpFft, float *k2, float *kappa, int N);

void c_after_tff(float *tmpTff, int N, float idivider);

void c_d_term(float *d_term, float *tmpTff, int N, float diffusion_p1, float diffusion_p2);

void c_T(float *T, float *d_term, int N, float dt);
void c_T(float *T, float *d_term, float *p_term, float *q_term, int N, float dt);

void c_cem43(int N, float *cem43, float *T, float dt);

// NO hOMOGENOUS

void c_perfusion_noh(int R, int C, float *p_term, float *T, float *kappa,
		FloatComplex *tmpFft, float *coef, float *temp, float idivider);

void c_fft_x_noh(int N, FloatComplex *fft, FloatComplex *tmpFft, FloatComplex *deriv);

void c_tff_x_noh(int N, float *tff, float idivider, float diffusion_p2);

void c_fft_x_2_noh(int N, FloatComplex *fft, FloatComplex *deriv);

void c_pre_d_term_noh(int N, float *tff, float idivider);

void c_d_term_noh(int N, float *d_term, float *diffusion_p1_noh, float *tff_1, float *tff_2);
void c_d_term_noh(int N, float *d_term, float *diffusion_p1_noh, float *tff_1, float *tff_2, float *tff_3);
void c_d_term_noh(int N, float *d_term, float diffusion_p1, float *tff_1, float *tff_2);
void c_d_term_noh(int N, float *d_term, float diffusion_p1, float *tff_1, float *tff_2, float *tff_3);

void c_T_noh(int N, float *T, float *p_term, float *q_term, float dt, float *d_term);


#endif

