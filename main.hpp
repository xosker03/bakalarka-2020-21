/**
 * @file      main.hpp
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Hlavičkový soubor main.cpp
 *
 * @version   heatsim 1.0
 *
 * @date      19.5.2021
 */

#ifndef _MAIN_HPP_
#define _MAIN_HPP_

#include <assert.h>
#include <fftw3.h>
#include <complex>

#if defined (GPU_ACC) || defined(GPU_CUDA)
	#include <cufft.h>
#endif

#include <complex>
#include <string>

using FloatComplex = std::complex<float>;
using dimensions = int*;

extern std::string PATH_INPUT;
extern std::string PATH_OUTPUT;

struct knownDim {
	int x;
	int y;
	int z;
};

extern dimensions FullDim;
extern dimensions ReducedDim;
extern struct knownDim kd;

extern int dimension_count;
extern bool use_perfusion;
extern int use_q_term;
extern bool homogenous;
extern int TIME;
extern bool homo_diffusion_p1;

#if defined (GPU_ACC) || defined(GPU_CUDA)
	#define get_index(x, y, z, d) ((x) + (y) * d[0] + (z) * d[0] * d[1])
#else
inline int get_index(int x, int y, int z, dimensions d){
	assert(x < d[kd.x]);
	assert(y < d[kd.y]);
	assert(z < d[kd.z]);
	return x + y * d[kd.x] + z * d[kd.x] * d[kd.y];
}
#endif


#if defined (GPU_ACC) || defined(GPU_CUDA)
	extern cufftHandle planR2C;
	extern cufftHandle planC2R;
#else
	extern fftwf_plan planR2C;
	extern fftwf_plan planC2R;
#endif


void fftR2C(float *a, FloatComplex *b);
void fftC2R(FloatComplex *a, float *b);

int get_size(dimensions d);


#endif
