/**
 * @file      gpu_cuda.cu
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Soubor implementace cuda kernelů pro hlavní výpočet
 *
 * @version   heatsim 1.0
 *
 * @date      19.5.2021
 */


#include "gpu_cuda.h"
#include <thrust/complex.h>
#include "main.hpp"

#define CUDA_WIDTH 1024

using thFloatComplex = thrust::complex<float>;

////////////////// HOMOGENOUS ////////////////////////

__global__ 
void __c_perfusion_1(int N, float *p_term, float *T, float temp){
	int i = threadIdx.x + blockDim.x * blockIdx.x;
	if (i < N) 
		p_term[i] = T[i] - temp;
}
__global__ 
void __c_perfusion_2(int N, thFloatComplex *tmpFft, float *kappa){
	int i = threadIdx.x + blockDim.x * blockIdx.x;
	if (i < N) 
		tmpFft[i] *= kappa[i];
}
__global__ 
void __c_perfusion_3(int N, float *p_term, float coef, float idivider){
	int i = threadIdx.x + blockDim.x * blockIdx.x;
	if (i < N) 
		p_term[i] *= -coef * idivider;
}

__global__ 
void __c_after_fft(thFloatComplex *tmpFft, float *k2, float *kappa, int N){
	int i = threadIdx.x + blockDim.x * blockIdx.x;
	if (i < N) 
		tmpFft[i] = -k2[i] * kappa[i] * tmpFft[i];
}


__global__ 
void __c_after_tff(float *tmpTff, int N, float idivider){
	int i = threadIdx.x + blockDim.x * blockIdx.x;
	if (i < N) 
		tmpTff[i] *= idivider;
}


__global__ 
void __c_d_term(float *d_term, float *tmpTff, int N, float diffusion_p1, float diffusion_p2){
	int i = threadIdx.x + blockDim.x * blockIdx.x;
	if (i < N) 
		d_term[i] = diffusion_p1 * diffusion_p2 * tmpTff[i];
}

__global__ 
void __c_T(float *T, float *d_term, int N, float dt){
	int i = threadIdx.x + blockDim.x * blockIdx.x;
	if (i < N) 
		T[i] += dt * d_term[i];
}
__global__ 
void __c_T(float *T, float *d_term, float *p_term, float *q_term, int N, float dt){
	int i = threadIdx.x + blockDim.x * blockIdx.x;
	if (i < N) 
		T[i] += dt * (d_term[i] + p_term[i] + q_term[i]);
}

__global__ 
void __c_cem43(int N, float *cem43, float *T, float dt){
	int i = threadIdx.x + blockDim.x * blockIdx.x;
	if (i < N){
		float tmp = 0.5 * (T[i] >= 43) + 0.25 * ((T[i] >= 37) & (T[i] < 43));
		float power = pow(tmp, 43.0 - T[i]);
		cem43[i] = cem43[i] + dt * (1.0/60) * power;	
	}
}


void c_perfusion(int R, int C, float *p_term, float *T, float *kappa,
		FloatComplex *tmpFft, float coef, float temp, float idivider)
{
	dim3 DimGrid_R((R-1)/CUDA_WIDTH + 1, 1, 1);
	dim3 DimBlock_R(CUDA_WIDTH,1,1);
	dim3 DimGrid_C((C-1)/CUDA_WIDTH + 1, 1, 1);
	dim3 DimBlock_C(CUDA_WIDTH,1,1);
	
	__c_perfusion_1<<<DimGrid_R, DimBlock_R>>>(R, p_term, T, temp);

	fftR2C(p_term, tmpFft);

	__c_perfusion_2<<<DimGrid_C, DimBlock_C>>>(C, (thFloatComplex*) tmpFft, kappa);

	fftC2R(tmpFft, p_term);
	
	__c_perfusion_3<<<DimGrid_R, DimBlock_R>>>(R, p_term, coef, idivider);

}


void c_after_fft(FloatComplex *tmpFft, float *k2, float *kappa, int N){
	dim3 DimGrid((N-1)/CUDA_WIDTH + 1, 1, 1);
	dim3 DimBlock(CUDA_WIDTH,1,1);

	__c_after_fft<<<DimGrid, DimBlock>>>((thFloatComplex*) tmpFft, k2, kappa, N);
}

void c_after_tff(float *tmpTff, int N, float idivider){
	dim3 DimGrid((N-1)/CUDA_WIDTH + 1, 1, 1);
	dim3 DimBlock(CUDA_WIDTH,1,1);

	__c_after_tff<<<DimGrid, DimBlock>>>(tmpTff, N, idivider);
}

void c_d_term(float *d_term, float *tmpTff, int N, float diffusion_p1, float diffusion_p2){
	dim3 DimGrid((N-1)/CUDA_WIDTH + 1, 1, 1);
	dim3 DimBlock(CUDA_WIDTH,1,1);

	__c_d_term<<<DimGrid, DimBlock>>>(d_term, tmpTff, N, diffusion_p1, diffusion_p2);
}

void c_T(float *T, float *d_term, int N, float dt){
	dim3 DimGrid((N-1)/CUDA_WIDTH + 1, 1, 1);
	dim3 DimBlock(CUDA_WIDTH,1,1);

	__c_T<<<DimGrid, DimBlock>>>(T, d_term, N, dt);
}
void c_T(float *T, float *d_term, float *p_term, float *q_term, int N, float dt){
	dim3 DimGrid((N-1)/CUDA_WIDTH + 1, 1, 1);
	dim3 DimBlock(CUDA_WIDTH,1,1);

	__c_T<<<DimGrid, DimBlock>>>(T, d_term, p_term, q_term, N, dt);
}


void c_cem43(int N, float *cem43, float *T, float dt){
	dim3 DimGrid((N-1)/CUDA_WIDTH + 1, 1, 1);
	dim3 DimBlock(CUDA_WIDTH,1,1);

	__c_cem43<<<DimGrid, DimBlock>>>(N, cem43, T, dt);
}


////////////////// NO HOMOGENOUS ////////////////////////


__global__ 
void __c_perfusion_1_noh(int N, float *p_term, float *T, float *temp){
	int i = threadIdx.x + blockDim.x * blockIdx.x;
	if (i < N) 
		p_term[i] = T[i] - temp[i];
}
__global__ 
void __c_perfusion_2_noh(int N, thFloatComplex *tmpFft, float *kappa){
	int i = threadIdx.x + blockDim.x * blockIdx.x;
	if (i < N) 
		tmpFft[i] *= kappa[i];
}
__global__ 
void __c_perfusion_3_noh(int N, float *p_term, float *coef, float idivider){
	int i = threadIdx.x + blockDim.x * blockIdx.x;
	if (i < N) 
		p_term[i] *= -coef[i] * idivider;
}

__global__
void __c_fft_x_noh(int N, thFloatComplex *fft, thFloatComplex *tmpFft, thFloatComplex *deriv){
	int i = threadIdx.x + blockDim.x * blockIdx.x;
	if (i < N)
		fft[i] = tmpFft[i] * deriv[i];
}


__global__
void __c_tff_x_noh(int N, float *tff, float idivider, float diffusion_p2){
	int i = threadIdx.x + blockDim.x * blockIdx.x;
	if (i < N) 
		tff[i] *= idivider * diffusion_p2;
}


__global__
void __c_fft_x_2_noh(int N, thFloatComplex *fft, thFloatComplex *deriv){
	int i = threadIdx.x + blockDim.x * blockIdx.x;
	if (i < N)
		fft[i] *= deriv[i];
}


__global__
void __c_pre_d_term_noh(int N, float *tff, float idivider){
	int i = threadIdx.x + blockDim.x * blockIdx.x;
	if (i < N) 
		tff[i] *= idivider;
}




__global__
void __c_d_term_noh(int N, float *d_term, float *diffusion_p1_noh, float *tff_1, float *tff_2){
	int i = threadIdx.x + blockDim.x * blockIdx.x;
	if (i < N)
		d_term[i] = diffusion_p1_noh[i] * ( (tff_1[i]) + (tff_2[i]));
}
__global__
void __c_d_term_noh(int N, float *d_term, float *diffusion_p1_noh, float *tff_1, float *tff_2, float *tff_3){
	int i = threadIdx.x + blockDim.x * blockIdx.x;
	if (i < N)
		d_term[i] = diffusion_p1_noh[i] * ( (tff_1[i]) + (tff_2[i]) + (tff_3[i]));
}
__global__
void __c_d_term_noh(int N, float *d_term, float diffusion_p1, float *tff_1, float *tff_2){
	int i = threadIdx.x + blockDim.x * blockIdx.x;
	if (i < N)
		d_term[i] = diffusion_p1 * ( (tff_1[i]) + (tff_2[i]));
}
__global__
void __c_d_term_noh(int N, float *d_term, float diffusion_p1, float *tff_1, float *tff_2, float *tff_3){
	int i = threadIdx.x + blockDim.x * blockIdx.x;
	if (i < N)
		d_term[i] = diffusion_p1 * ( (tff_1[i]) + (tff_2[i]) + (tff_3[i]));
}




__global__
void __c_T_noh(int N, float *T, float *p_term, float *q_term, float dt, float *d_term){
	int i = threadIdx.x + blockDim.x * blockIdx.x;
	if (i < N) 
		T[i] += dt * (d_term[i] + p_term[i] + q_term[i]);
}





void c_perfusion_noh(int R, int C, float *p_term, float *T, float *kappa,
		FloatComplex *tmpFft, float *coef, float *temp, float idivider)
{
	dim3 DimGrid_R((R-1)/CUDA_WIDTH + 1, 1, 1);
	dim3 DimBlock_R(CUDA_WIDTH,1,1);
	dim3 DimGrid_C((C-1)/CUDA_WIDTH + 1, 1, 1);
	dim3 DimBlock_C(CUDA_WIDTH,1,1);
	
	__c_perfusion_1_noh<<<DimGrid_R, DimBlock_R>>>(R, p_term, T, temp);

	fftR2C(p_term, tmpFft);

	__c_perfusion_2_noh<<<DimGrid_C, DimBlock_C>>>(C, (thFloatComplex*) tmpFft, kappa);

	fftC2R(tmpFft, p_term);
	
	__c_perfusion_3_noh<<<DimGrid_R, DimBlock_R>>>(R, p_term, coef, idivider);

}





void c_fft_x_noh(int N, FloatComplex *fft, FloatComplex *tmpFft, FloatComplex *deriv){
	dim3 DimGrid((N-1)/CUDA_WIDTH + 1, 1, 1);
	dim3 DimBlock(CUDA_WIDTH,1,1);

	__c_fft_x_noh<<<DimGrid, DimBlock>>>(N, 
			(thFloatComplex*) fft, (thFloatComplex*) tmpFft, (thFloatComplex*) deriv);
}

void c_tff_x_noh(int N, float *tff, float idivider, float diffusion_p2){
	dim3 DimGrid((N-1)/CUDA_WIDTH + 1, 1, 1);
	dim3 DimBlock(CUDA_WIDTH,1,1);

	__c_tff_x_noh<<<DimGrid, DimBlock>>>(N, tff, idivider, diffusion_p2);
}


void c_fft_x_2_noh(int N, FloatComplex *fft, FloatComplex *deriv){
	dim3 DimGrid((N-1)/CUDA_WIDTH + 1, 1, 1);
	dim3 DimBlock(CUDA_WIDTH,1,1);

	__c_fft_x_2_noh<<<DimGrid, DimBlock>>>(N, (thFloatComplex*) fft, (thFloatComplex*) deriv);
}


void c_pre_d_term_noh(int N, float *tff, float idivider){
	dim3 DimGrid((N-1)/CUDA_WIDTH + 1, 1, 1);
	dim3 DimBlock(CUDA_WIDTH,1,1);

	__c_pre_d_term_noh<<<DimGrid, DimBlock>>>(N, tff, idivider);
}





void c_d_term_noh(int N, float *d_term, float *diffusion_p1_noh, float *tff_1, float *tff_2){
	dim3 DimGrid((N-1)/CUDA_WIDTH + 1, 1, 1);
	dim3 DimBlock(CUDA_WIDTH,1,1);

	__c_d_term_noh<<<DimGrid, DimBlock>>>(N, d_term, diffusion_p1_noh, tff_1, tff_2);
}
void c_d_term_noh(int N, float *d_term, float *diffusion_p1_noh, float *tff_1, float *tff_2, float *tff_3){
	dim3 DimGrid((N-1)/CUDA_WIDTH + 1, 1, 1);
	dim3 DimBlock(CUDA_WIDTH,1,1);

	__c_d_term_noh<<<DimGrid, DimBlock>>>(N, d_term, diffusion_p1_noh, tff_1, tff_2, tff_3);
}
void c_d_term_noh(int N, float *d_term, float diffusion_p1, float *tff_1, float *tff_2){
	dim3 DimGrid((N-1)/CUDA_WIDTH + 1, 1, 1);
	dim3 DimBlock(CUDA_WIDTH,1,1);

	__c_d_term_noh<<<DimGrid, DimBlock>>>(N, d_term, diffusion_p1, tff_1, tff_2);
}
void c_d_term_noh(int N, float *d_term, float diffusion_p1, float *tff_1, float *tff_2, float *tff_3){
	dim3 DimGrid((N-1)/CUDA_WIDTH + 1, 1, 1);
	dim3 DimBlock(CUDA_WIDTH,1,1);

	__c_d_term_noh<<<DimGrid, DimBlock>>>(N, d_term, diffusion_p1, tff_1, tff_2, tff_3);
}






void c_T_noh(int N, float *T, float *p_term, float *q_term, float dt, float *d_term){
//void c_T_noh(int N, float *T, float dt, float *d_term){
	dim3 DimGrid((N-1)/CUDA_WIDTH + 1, 1, 1);
	dim3 DimBlock(CUDA_WIDTH,1,1);

	__c_T_noh<<<DimGrid, DimBlock>>>(N, T, p_term, q_term, dt, d_term);
}

