/**
 * @file      zaklad.hpp
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Hlavičkový soubor užitečných funkcí pro užívání linux shellu
 *
 * @version   heatsim 1.0
 *
 * @date      19.5.2021
 */


#ifndef __ZAKLAD_H__
#define __ZAKLAD_H__

#include <string>


struct bcolors{
	char *BLUE;
	char *GREEN;
	char *RED;
	char *VIOLET;
	char *YELLOW;
	char *LCYAN;
	char *BOLD;
	char *UNDERLINE;
	char *CRITIC;
	char *ENDC;
	char *CURSORH;
	char *CURSORS;
	char *chyba;
};

extern struct bcolors bc;

std::string shell_pipe(const std::string cmd, int& out_exitStatus);
int shell_call(const std::string cmd);
void shell_throw(const std::string cmd);


#endif
