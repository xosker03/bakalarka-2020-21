/**
 * @file      timer.h
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Hlavičkový soubor timer pro měření času
 *
 * @version   heatsim 1.0
 *
 * @date      19.5.2021
 */


#ifndef TIMER_H_
#define TIMER_H_

#include <sys/time.h>


#define timerStart() gettimeofday(&start, NULL)
#define timerStop() gettimeofday(&stop, NULL)
#define timer_us ((1000000 * stop.tv_sec + stop.tv_usec) - (1000000 * start.tv_sec + start.tv_usec))

extern struct timeval stop, start;

#endif
