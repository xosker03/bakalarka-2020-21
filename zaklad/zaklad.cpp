/**
 * @file      zaklad.cpp
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Soubor implementace užitečných funkcí pro užívání linux shellu
 *
 * @version   heatsim 1.0
 *
 * @date      19.5.2021
 */

#include "zaklad.hpp"
#include <stdio.h>
#include <string>
#include <array>
#include <iostream>
#include <thread>

struct bcolors bc = {
	.BLUE = "\033[94m",
	.GREEN = "\033[92m",
	.RED = "\033[91m",
	.VIOLET = "\033[95m",
	.YELLOW = "\033[93m",
	.LCYAN = "\033[96m",
	.BOLD = "\033[1m",
	.UNDERLINE = "\033[4m",
	.CRITIC = "\033[41;1m",
	.ENDC = "\033[0m",
	.CURSORH = "\033[?25l",
	.CURSORS = "\033[?25h",
	.chyba = "\033[91mChyba: \033[0m"
};


#ifndef ONLY_COLORS

std::string shell_pipe(const std::string cmd, int& out_exitStatus)
{
    out_exitStatus = 0;
    auto pPipe = ::popen(cmd.c_str(), "r");
    if(pPipe == nullptr){
        throw std::string("Cannot open pipe");
    }

    std::array<char, 256> buffer;

    std::string result;

    while(not std::feof(pPipe)){
        auto bytes = std::fread(buffer.data(), 1, buffer.size(), pPipe);
        result.append(buffer.data(), bytes);
    }

    auto rc = ::pclose(pPipe);

    if(WIFEXITED(rc)){
        out_exitStatus = WEXITSTATUS(rc);
    }

    return result;
}


int shell_call(const std::string cmd){
	std::cout << bc.BLUE << cmd << bc.ENDC << std::endl;
    auto pPipe = ::popen((cmd).c_str(), "r");
    if(pPipe == nullptr){
        throw std::string("Cannot open pipe");
    }
	char tmp[1024];
    while(not std::feof(pPipe)){
        std::fread(tmp, 1, 1024, pPipe);
    }
    int rc = ::pclose(pPipe);
	return rc;
}

void shell_throw(const std::string cmd){
	std::thread first (shell_call, cmd + " 2>&1");
	first.detach();
}



#endif
