/**
 * @file      helpers.cpp
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Soubor implementace pomocných funkcí pro ladění heatsim
 *
 * @version   heatsim 1.0
 *
 * @date      19.5.2021
 */


#include "helpers.hpp"
#include "main.hpp"
#include "zaklad/zaklad.hpp"
#include <stdio.h>
#include <string>

using namespace std;

/*
 *	Funkce slouží pro ladění programu, kdy uloží matici do txt souboru
 */
void print_mat(float *m, char *name, char *color, int file, bool full){
	if(m == NULL)
		return;
	//copyAllToHost();
	FILE *F = stdout;

	if(file == 1){
		F = stderr;
		color = "";
	}

	if(file == 2){
		string tmp = "data/" + string(name);
		F = fopen(tmp.c_str(), "w");
		color = "";
	}

	fprintf(stdout, "\n%s%s\n", color, name);
	if(!full)
	{
		for (int z = 0; z < 1; z++)
		//for (int z = 0; z < ReducedDim[kd.z]; z++)
		{
			for (int y = 0; y < ReducedDim[kd.y]; y++)
			{
				for (int x = 0; x < ReducedDim[kd.x];  x++)
				{
					int i = get_index(x, y, z, ReducedDim);
					fprintf(F, "%e ", m[i]);
				}
				fprintf(F, "\n");
			}
		}
	} else {
		for (int z = 0; z < 1; z++)
		//for (int z = 0; z < FullDim[kd.z]; z++)
		{
			for (int y = 0; y < FullDim[kd.y]; y++)
			{
				for (int x = 0; x < FullDim[kd.x];  x++)
				{
					int i = get_index(x, y, z, FullDim);
					fprintf(F, "%e ", m[i]);
				}
				fprintf(F, "\n");
			}
		}
	}
	if(file == 2)
		fclose(F);
	fprintf(stdout, bc.ENDC);
}


void print_mat(FloatComplex *m, char *name, char *color, int file, bool full){
	if(m == NULL)
		return;
	//copyAllToHost();
	FILE *F = stdout;
	FILE *F2 = NULL;

	if(file == 1){
		F = stderr;
		//color = "";
	}

	if(file == 2){
		string tmp = "data/" + string(name);
		string tmp2 = "data/" + string(name) + "_imag";
		F = fopen(tmp.c_str(), "w");
		F2 = fopen(tmp2.c_str(), "w");
		//color = "";
	}

	fprintf(stdout, "\n%s%s\n", color, name);
	if(!full)
	{
		for (int z = 0; z < 1; z++)
		//for (int z = 0; z < ReducedDim[kd.z]; z++)
		{
			for (int y = 0; y < ReducedDim[kd.y]; y++)
			{
				for (int x = 0; x < ReducedDim[kd.x];  x++)
				{
					int i = get_index(x, y, z, ReducedDim);
					fprintf(F, "%e ", m[i].real());
					if(file == 2)
						fprintf(F2, "%e ", m[i].imag());
				}
				fprintf(F, "\n");
				if(file == 2)
					fprintf(F2, "\n");
			}
		}
	} else {
		for (int z = 0; z < 1; z++)
		//for (int z = 0; z < FullDim[kd.z]; z++)
		{
			for (int y = 0; y < FullDim[kd.y]; y++)
			{
				for (int x = 0; x < FullDim[kd.x];  x++)
				{
					int i = get_index(x, y, z, FullDim);
					fprintf(F, "%e ", m[i].real());
					if(file == 2)
						fprintf(F2, "%e ", m[i].imag());
				}
				fprintf(F, "\n");
				if(file == 2)
					fprintf(F2, "\n");
			}
		}
	}


	if(file == 2){
		fclose(F);
		fclose(F2);
	}
	fprintf(stdout, bc.ENDC);
}

