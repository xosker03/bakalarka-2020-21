/*  OpenACC micro benchmark
 *  Author: Josef Oškera
 *  Compiled: pgc++ 19.10-0, gcc 10.2.0
 *  Date: 10.12.2020
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <signal.h>

#include <iostream>
#include <string>
#include <map>

#include "mat.hpp"
#include "logger.hpp"

using namespace std;

//Matrix start size
constexpr uint64_t START_SIZE = 1024;
//if last test exceeds this time, test will be stopped
//to avoid this you have 2 possibilities, 1. increase this constant, 2. decrease number of iterations
constexpr uint64_t MAX_TIME = 60;
//Makefile counts with this preset names
//rosults with Matrix size in MB
string FOLDER = "./results";
//results with Matrix width in fields
string FOLDER2 = "./results2";

string NAME;
string SUFFIX;


FILE *result;
FILE *result2;
struct timeval stop, start;


struct test {
	string name;
    string header;
    string header2;
	double (*test)(uint64_t, uint64_t, struct implement *);
    std::vector<struct implement> *imps;
};

map<uint64_t, struct test> tests = {
	{ MAT_MEMCPY, (struct test) {"matMemcpy", "#MB GB/s", "#width GB/s", matMemcpy, NULL} },
	{ MAT_ADD, (struct test) {"matAdd", "#MB GFlops", "#width GFlops", matAdd, &matAddImp} },
	{ MAT_MUL, (struct test) {"matMul", "#MB GFlops", "#width GFlops", matMul, &matMulImp} },
	{ MAT_TRANS, (struct test) {"matTrans", "#MB GB/s", "#width GB/s", matTrans, &matTransImp} },
	{ MAT_JACOBI, (struct test) {"matJacobi", "#MB GFlops", "#width GFlops", matJacobi, &matJacobiImp} },
};


void handle_sigint(int sig)
{
    printf("KONEC\n");
    if(result){
        fclose(result);
    }
    if(result2){
    	fclose(result2);
    }
    loggerClose();
    exit(1);
}

//fce increasing Matrix size in for cycles
inline uint64_t inc(uint64_t num)
{
    //return num * 2;
    return num + num * 0.1;
}

int main(int argc, char **argv)
{

	std::cout.sync_with_stdio(true);
	std::cerr.sync_with_stdio(true);

    signal(SIGINT, handle_sigint);

    for(int i = 1; i < argc; ++i)
    {
        if(string(argv[i]) == "--help")
        {
            printf("tester NAME COMMAND SIZE ITER\n");
            printf("\tNAME - appedix for test name (serult file will be for matAdd test: matAdd_{NAME})\n");
            printf("\tCOMMAND - command mask, mask you can get in \"mat.hpp\"\n");
            printf("\tSIZE - maximum size of 1 matrix in MB (warning for example in matMull program alloc 3 matrixes each of max size SIZE)\n");
            printf("\tITER - number of iterations\n\n");
            printf("\tfor now maximum runtime for each test is 60s\n");
            printf("\toutput folder is \"results\" and \"results2\"\n");
            printf("\tresults contains sizes in MB\n");
            printf("\tresults2 contains sizes in num of fields\n");
            return 0;
        }
    }

    if(argc != 5)
    {
        printf("Incorrect number of arguments\n\n");
        printf("tester NAME COMMAND SIZE ITER\n\n");
        printf("for full help type: tester --help\n");
        return 1;
    }

    char *endptr;
    uint64_t command = strtol(argv[2], &endptr, 10);
    if(*endptr != '\0')
    {
        printf("COMMAND is not a valid number\n");
        return 1;
    }
    double size = strtod(argv[3], &endptr) * 1024 * 1024;
    if(*endptr != '\0')
    {
        printf("SIZE is not a valid number\n");
        return 1;
    }
    uint64_t iter = strtoul(argv[4], &endptr, 10);
    if(*endptr != '\0')
    {
        printf("ITER is not a valid number\n");
        return 1;
    }


    printf("TEST %s, COMM %x, SIZE %lf MB, ITER %d\n", argv[1], command, (size / 1024) / 1024, iter);

    double ret = 0;
	
	SUFFIX = string(argv[1]);


    //tests for tests (implementation testing)
	map<uint64_t, struct test>::iterator it = tests.begin();
    if(command & MAT_AUTO)
		for(; it != tests.end(); ++it){
			cout << "available   " << it->first << "  " << it->second.name << endl;

			struct test *target = &it->second;

			if(command & it->first)
			{
				cout << "\trunning test: " << target->name << endl;
				if (target->imps){
					//for (struct implement a : target->imps){
					for (int i = 0; i < target->imps->size(); ++i){
						struct implement *a = &(*target->imps)[i];
						cout << "\t\trunning implementation: " << a->name << endl;
						if(!a->disabled){
							loggerSetup(target->name, SUFFIX + "_" + a->name, target->header, target->header2, true);
							loggerSetImpName(a->name);
							for(uint64_t s = START_SIZE; s <= size; s = inc(s))
							{
								ret = target->test(s, iter, a);
								if(ret > MAX_TIME)
								{
									break;
								}
							}
							loggerClose();
							loggerDebug();
						}
					}
				}
			}
		}
    loggerDebug();



    //individual tests
    it = tests.begin();
	for(; it != tests.end(); ++it){
		cout << "Available   " << it->first << "  " << it->second.name << endl;

		struct test *target = &it->second;

		if(command & it->first)
		{
			cout << "\tRunning test: " << target->name << endl;
			if (target->imps)
				cout << "\t\tRunning implementation: " << (*target->imps)[0].name << endl;
			else
				cout << "\t\tRunning implementation: " << "NULL" << endl;
			loggerSetup(target->name, SUFFIX, target->header, target->header2, false);
			for(uint64_t s = START_SIZE; s <= size; s = inc(s))
			{
				if (target->imps)
					ret = target->test(s, iter, &(*target->imps)[0]);
				else
					ret = target->test(s, iter, NULL);
				if(ret > MAX_TIME)
				{
					break;
				}
			}
			loggerClose();
		}
	}


    return 0;
}


