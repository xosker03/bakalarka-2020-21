/*  OpenACC micro benchmark
 *  Author: Josef Oškera
 *  Compiled: pgc++ 19.10-0, gcc 10.2.0
 *  Date: 10.12.2020
 */

#include "mat.hpp"
#include "logger.hpp"

void matMulAuto(struct matParams par);
void matMulSeq(struct matParams par);
void matMulSeqTile32(struct matParams par);
void matMulSeqTile64(struct matParams par);
void matMulReduction(struct matParams par);
void matMulKernels(struct matParams par);
void matMulSup(struct matParams par);


std::vector<struct implement> matMulImp = {
	{ "mat_mul_auto", (void *) matMulAuto, true },
	{ "mat_mul_seq", (void *) matMulSeq, false },
  { "mat_mul_seq-tile32", (void *) matMulSeqTile32, false },
  { "mat_mul_seq-tile64", (void *) matMulSeqTile64, false },
  { "mat_mul_reduction", (void *) matMulReduction, false },
  { "mat_mul_kernels", (void *) matMulKernels, true },
  { "mat_mul_sup", (void *) matMulSup, false }
};

void matMulAuto(struct matParams par)
{
	void (*implementation)(struct matParams) = (void (*)(struct matParams)) loggerGetBestImp(par.mat_side);
    if (!implementation){
        implementation = (void (*)(struct matParams)) matMulImp[1].func; //matMulBasic nebo cokoliv jiného
    }
	implementation(par);
}
//-----------------------------------------------------------------------------------
void matMulSeq(struct matParams par)
{
    uint64_t iter = par.iter;
    uint64_t mat_side = par.mat_side;

    //support pointer to matrix data
    float *mt_a = par.mat_a->data();
    float *mt_b = par.mat_b->data();
    float *mt_c = par.mat_c->data();

    //#pragma acc data copy(mt_a[0:mat_size], mt_b[0:mat_size], mt_c[0:mat_size])
	#pragma acc data present(mt_a, mt_b, mt_c)
    {
        timerStart();
        for(uint64_t j = 0; j < iter; ++j)
        {
            #pragma acc parallel loop
            for(uint64_t y = 0; y < mat_side; ++y)
            {
                #pragma acc loop
                for(uint64_t x = 0; x < mat_side; ++x)
                {
                    float sum = 0;
                    #pragma acc loop seq
                    for(uint64_t i = 0; i < mat_side; ++i)
                    {
                        sum += mt_a[i + y * mat_side] * mt_b[x + i * mat_side];
                    }
                    mt_c[x + y * mat_side] = sum;
                }
            }
        }
        timerStop();
    }
}
//-----------------------------------------------------------------------------------
void matMulSeqTile32(struct matParams par)
{
    uint64_t iter = par.iter;
    uint64_t mat_side = par.mat_side;

    //support pointer to matrix data
    float *mt_a = par.mat_a->data();
    float *mt_b = par.mat_b->data();
    float *mt_c = par.mat_c->data();

    //#pragma acc data copy(mt_a[0:mat_size], mt_b[0:mat_size], mt_c[0:mat_size])
  #pragma acc data present(mt_a, mt_b, mt_c)
    {
        timerStart();
        for(uint64_t j = 0; j < iter; ++j)
        {
            #pragma acc parallel loop tile(32)
            for(uint64_t y = 0; y < mat_side; ++y)
            {
                #pragma acc loop
                for(uint64_t x = 0; x < mat_side; ++x)
                {
                    float sum = 0;
                    #pragma acc loop seq
                    for(uint64_t i = 0; i < mat_side; ++i)
                    {
                        sum += mt_a[i + y * mat_side] * mt_b[x + i * mat_side];
                    }
                    mt_c[x + y * mat_side] = sum;
                }
            }
        }
        timerStop();
    }
}
//-----------------------------------------------------------------------------------
void matMulSeqTile64(struct matParams par)
{
    uint64_t iter = par.iter;
    uint64_t mat_side = par.mat_side;

    //support pointer to matrix data
    float *mt_a = par.mat_a->data();
    float *mt_b = par.mat_b->data();
    float *mt_c = par.mat_c->data();

    //#pragma acc data copy(mt_a[0:mat_size], mt_b[0:mat_size], mt_c[0:mat_size])
  #pragma acc data present(mt_a, mt_b, mt_c)
    {
        timerStart();
        for(uint64_t j = 0; j < iter; ++j)
        {
            #pragma acc parallel loop tile(64)
            for(uint64_t y = 0; y < mat_side; ++y)
            {
                #pragma acc loop
                for(uint64_t x = 0; x < mat_side; ++x)
                {
                    float sum = 0;
                    #pragma acc loop seq
                    for(uint64_t i = 0; i < mat_side; ++i)
                    {
                        sum += mt_a[i + y * mat_side] * mt_b[x + i * mat_side];
                    }
                    mt_c[x + y * mat_side] = sum;
                }
            }
        }
        timerStop();
    }
}
//-----------------------------------------------------------------------------------
void matMulReduction(struct matParams par)
{
    uint64_t iter = par.iter;
    uint64_t mat_side = par.mat_side;

    //support pointer to matrix data
    float *mt_a = par.mat_a->data();
    float *mt_b = par.mat_b->data();
    float *mt_c = par.mat_c->data();

    //#pragma acc data copy(mt_a[0:mat_size], mt_b[0:mat_size], mt_c[0:mat_size])
  #pragma acc data present(mt_a, mt_b, mt_c)
    {
        timerStart();
        for(uint64_t j = 0; j < iter; ++j)
        {
            #pragma acc parallel loop collapse(2)
            for(uint64_t y = 0; y < mat_side; ++y)
            {
                for(uint64_t x = 0; x < mat_side; ++x)
                {
                    float sum = 0;
                    #pragma acc loop reduction(+:sum)
                    for(uint64_t i = 0; i < mat_side; ++i)
                    {
                        sum += mt_a[i + y * mat_side] * mt_b[x + i * mat_side];
                    }
                    mt_c[x + y * mat_side] = sum;
                }
            }
        }
        timerStop();
    }
}
//-----------------------------------------------------------------------------------
void matMulKernels(struct matParams par)
{
    uint64_t iter = par.iter;
    uint64_t mat_side = par.mat_side;

    //support pointer to matrix data
    float *mt_a = par.mat_a->data();
    float *mt_b = par.mat_b->data();
    float *mt_c = par.mat_c->data();

    //#pragma acc data copy(mt_a[0:mat_size], mt_b[0:mat_size], mt_c[0:mat_size])
  #pragma acc data present(mt_a, mt_b, mt_c)
    {
        timerStart();
        for(uint64_t j = 0; j < iter; ++j)
        {
            #pragma acc kernels
            for(uint64_t y = 0; y < mat_side; ++y)
            {
                for(uint64_t x = 0; x < mat_side; ++x)
                {
                    float sum = 0;
                    for(uint64_t i = 0; i < mat_side; ++i)
                    {
                        sum += mt_a[i + y * mat_side] * mt_b[x + i * mat_side];
                    }
                    mt_c[x + y * mat_side] = sum;
                }
            }
        }
        timerStop();
    }
}
//-----------------------------------------------------------------------------------
void matMulSup(struct matParams par)
{
    uint64_t iter = par.iter;
    uint64_t mat_side = par.mat_side;

    //support pointer to matrix data
    float *mt_a = par.mat_a->data();
    float *mt_b = par.mat_b->data();
    float *mt_c = par.mat_c->data();

    //#pragma acc data copy(mt_a[0:mat_size], mt_b[0:mat_size], mt_c[0:mat_size])
  #pragma acc data present(mt_a, mt_b, mt_c)
    {
        timerStart();
        for(uint64_t j = 0; j < iter; ++j)
        {
            #pragma acc parallel loop gang vector_length(128) collapse(2)
            for(uint64_t y = 0; y < mat_side; ++y)
            {
                for(uint64_t x = 0; x < mat_side; ++x)
                {
                    float sum = 0;
                    #pragma acc loop vector reduction(+:sum)
                    for(uint64_t i = 0; i < mat_side; ++i)
                    {
                        sum += mt_a[i + y * mat_side] * mt_b[x + i * mat_side];
                    }
                    mt_c[x + y * mat_side] = sum;
                }
            }
        }
        timerStop();
    }
}

