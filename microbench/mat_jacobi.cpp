/*  OpenACC micro benchmark
 *  Author: Josef Oškera
 *  Compiled: pgc++ 19.10-0, gcc 10.2.0
 *  Date: 10.12.2020
 */

#include "mat.hpp"
#include <math.h>
#include "logger.hpp"

//Jacobi Iteration
//code from https://developer.nvidia.com/blog/openacc-example-part-1/

//arguments:    size - maximal size of 1 matrix in bajts
//              iter - number of iterations for more accurate results
//returns: test time in seconds
double matJacobi(uint64_t size, uint64_t iter, struct implement *imp)
{
    //side of matrix is sqrt(max size / sizeof(float))
    uint64_t mat_sizeof = size;// / 2;
    uint64_t mat_size = mat_sizeof / sizeof(float);
    uint64_t mat_side = sqrt(mat_size);
    mat_size = mat_side * mat_side;
    mat_sizeof = mat_size * sizeof(float);

    //Matrices allocations
    GpuMem<float> mat_a(mat_size);
    GpuMem<float> mat_b(mat_size);

    mat_a.cpyTD();
    mat_b.cpyTD();

    //Test section
	struct matParams par = {
		mat_side,
		iter,
		&mat_a,
		&mat_b,
		NULL
	};

	//Test section
	uint64_t (*implementation)(struct matParams par) = (uint64_t (*)(struct matParams par)) imp->func;
	uint64_t it = implementation(par);

    //result print
    double gflops = (it * mat_size * 5) / (((double)timer_us) / 1000000);
    gflops /= 1000000000;
    logger(mat_side, gflops, "matJacobi", imp);
    printf("\n");
    
    return (((double)timer_us) / 1000000);
}
