/*  OpenACC micro benchmark
 *  Author: Josef Oškera
 *  Compiled: pgc++ 19.10-0, gcc 10.2.0
 *  Date: 10.12.2020
 */

#include "mat.hpp"
#include <math.h>

#include "logger.hpp"

//Matrices multiplication C = A * B
//arguments:    size - maximal size of 1 matrix in bajts
//              iter - number of iterations for more accurate results
//returns: test time in seconds
double matMul(uint64_t size, uint64_t iter, struct implement *imp)
{
    //side of matrix is sqrt(max size / sizeof(float))
    uint64_t mat_sizeof = size;
    uint64_t mat_size = mat_sizeof / sizeof(float);
    uint64_t mat_side = sqrt(mat_size);
    mat_size = mat_side * mat_side;
    mat_sizeof = mat_size * sizeof(float);

    //Matrices allocations
    GpuMem<float> mat_a(mat_size);
    GpuMem<float> mat_b(mat_size);
    GpuMem<float> mat_c(mat_size);

    //test data
    for(uint64_t i = 0; i < mat_side; ++i)
    {
        for(uint64_t j = 0; j < mat_side; ++j)
            mat_a[j + i * mat_side] = 1;
    }
    for(uint64_t i = 0; i < mat_side; ++i)
    {
        mat_b[i * mat_side] = 1;
    }

    //data copy to device (if you don't use CPU)
	mat_a.cpyTD();
	mat_b.cpyTD();
	mat_c.cpyTD();

    //Test section
    struct matParams par = {
        mat_side,
        iter,
        &mat_a,
        &mat_b,
        &mat_c
    };

    //Test section
	void (*implementation)(struct matParams par) = (void (*)(struct matParams par)) imp->func;
	implementation(par);

    //result print
    double gflops = (iter * (mat_size * mat_side * 2)) / (((double)timer_us) / 1000000);
    gflops /= 1000000000;
    logger(mat_side, gflops, "matMul", imp);


    //verification
	mat_c.cpyTH();
    GpuMem<float> mat_res(mat_size);
    for(uint64_t i = 0; i < mat_side; ++i)
    {
        mat_res[i * mat_side] = mat_side;
    }
    if(mat_c == mat_res)
    {
        printf("GOOD");
    }
    else
    {
        printf("BAD");
        printf("\t%lu %lu", mat_c[0], mat_res[0]);
    }

    printf("\n");
    return (((double)timer_us) / 1000000);
}
