/*  OpenACC micro benchmark
 *  Author: Josef Oškera
 *  Compiled: pgc++ 19.10-0, gcc 10.2.0
 *  Date: 10.12.2020
 */

#ifndef TIMER_H_
#define TIMER_H_

#include <sys/time.h>


#define timerStart() gettimeofday(&start, NULL)
#define timerStop() gettimeofday(&stop, NULL)
#define timer_us ((1000000 * stop.tv_sec + stop.tv_usec) - (1000000 * start.tv_sec + start.tv_usec))

extern struct timeval stop, start;

#endif
