/*  OpenACC micro benchmark
 *  Author: Josef Oškera
 *  Compiled: pgc++ 19.10-0, gcc 10.2.0
 *  Date: 10.12.2020
 */

#ifndef _MAT_H_
#define _MAT_H_

#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <vector>
#include "timer.h"
#include "templates.hpp"

//this macros explain what will be COMMAND argument for every test
#define MAT_MEMCPY (1 << 0)         //1
#define MAT_ADD (1 << 1)            //2
#define MAT_MUL (1 << 2)            //4
#define MAT_TRANS (1 << 3)          //8
#define MAT_JACOBI (1 << 4)         //16
#define MAT_AUTO (1 << 5)           //32

extern FILE *result;
extern FILE *result2;

double matMemcpy(uint64_t size, uint64_t iter, struct implement *imp);
double matAdd(uint64_t size, uint64_t iter, struct implement *imp);
double matMul(uint64_t size, uint64_t iter, struct implement *imp);
double matJacobi(uint64_t size, uint64_t iter, struct implement *imp);
double matTrans(uint64_t size, uint64_t iter, struct implement *imp);

struct implement {
	const char *name;
	void *func;
    bool disabled;
};

//Implemetation
extern std::vector<struct implement> matAddImp;
extern std::vector<struct implement> matMulImp;
extern std::vector<struct implement> matTransImp;
extern std::vector<struct implement> matJacobiImp;


struct matParams {
    uint64_t mat_side;
    uint64_t iter;
    GpuMem<float> * mat_a;
    GpuMem<float> * mat_b;
    GpuMem<float> * mat_c;
};




#endif
