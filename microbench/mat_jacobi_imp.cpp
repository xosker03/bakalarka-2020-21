/*  OpenACC micro benchmark
 *  Author: Josef Oškera
 *  Compiled: pgc++ 19.10-0, gcc 10.2.0
 *  Date: 10.12.2020
 */

#include "mat.hpp"
#include "logger.hpp"

uint64_t matJacobiAuto(struct matParams par);
uint64_t matJacobiBasic(struct matParams par);


std::vector<struct implement> matJacobiImp = {
	{ "mat_jacobi_auto", (void *) matJacobiAuto, true },
	{ "mat_jacobi_basic", (void *) matJacobiBasic, false }
};

uint64_t matJacobiAuto(struct matParams par)
{
	uint64_t (*implementation)(struct matParams) = (uint64_t (*)(struct matParams)) loggerGetBestImp(par.mat_side);
    if (!implementation){
    	implementation = (uint64_t (*)(struct matParams)) matJacobiImp[1].func; //implementation = matJacobiBasic;
    }
	return implementation(par);
}

uint64_t matJacobiBasic(struct matParams par)
{
    double err = 1;
    uint64_t it = 0;
    double tol = 0.001;

    int n = par.mat_side;
    int m = par.mat_side;
    uint64_t iter = par.iter;

    //support pointer to matrix data
    float *Anew = par.mat_a->data();
    float *A = par.mat_b->data();

    #pragma acc data present(A, Anew)
    {
    	timerStart();
    	while( err > tol && it < iter )
		{
			#pragma acc parallel loop reduction(max:err) collapse(2) //vector_length(1024)
			for( int j = 1; j < n - 1; j++ )
			{
				for( int i = 1; i < m - 1; i++ )
				{
					Anew[j * m + i] = 0.25 * (A[j * m + i + 1] + A[j * m + i - 1] +
											  A[(j - 1) * m + i] + A[(j + 1) * m + i]);
					err = fmax(err, abs(Anew[j * m + i] - A[j * m + i]));
				}
			}

			#pragma acc parallel loop collapse(2)
			for( int j = 1; j < n - 1; j++ )
			{
				for( int i = 1; i < m - 1; i++ )
				{
					A[j * m + i] = Anew[j * m + i];
				}
			}
			it++;
		}
    	timerStop();
    }
    return it;
}
