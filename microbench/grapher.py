#!/bin/env python3

from os import listdir
from os.path import isfile, join, isdir
import matplotlib.pyplot as plt
import numpy as np
import sys

RESULTS = "./results"
MAT = []
GMAT = []
colors = ["blue", "red", "green", "violet", "yellow", "black", "cyan", "gray", "brown", "orange"]
styles = ["solid", "dashed", "dotted"]
color = 0

if len(sys.argv) > 1:
    RESULTS = sys.argv[1]

print(RESULTS)

class mat_data:
    def __init__(self, name, conf):
        tmp = name.split('_')
        col = len(tmp)
        #self.name = tmp[0] + "_" + tmp[1]
        self.name = tmp[0]
        for i in range(1, col - 1):
            self.name += "_" + tmp[i]
        #self.type = tmp[2]
        #tmp.pop(0)
        #tmp.pop(0)
        self.type = tmp[col - 1]
        try:
            self.axis = conf[0].strip('#').strip('\n').split(' ')
        except:
            pass
        conf.pop(0)
        self.data = []
        for line in conf:
            try:
                self.data.append(line.strip('\n').rstrip().split(' '))
            except:
                pass
        self.data_x = []
        self.data_y = []
        for i in self.data:
            self.data_x.append(float(i[0]))
            self.data_y.append(float(i[1]))

konfigy = [f for f in listdir(RESULTS) if isfile(join(RESULTS, f))]

for k in konfigy:
    file1 = open(RESULTS + "/" + k, 'r')
    Lines = file1.readlines()
    try:
        MAT.append(mat_data(k, Lines))
    except:
        pass


for i in MAT:
    print(i.name, i.type)

#print("/////////////////////////////////////////")

#print(MAT[0].name)
#print(MAT[0].type)
#print(MAT[0].axis)

#for i in MAT[0].data:
#    print(i)




def plot(mat, scale='linear'):
    plt.figure(mat[0].name + "_" + scale)
    plt.axhline(0, color='black')
    plt.grid(alpha=0.5, linestyle='--')
    plt.xlabel(mat[0].axis[0])
    plt.ylabel(mat[0].axis[1])
    plt.title(mat[0].name + " (" + scale + ")") 
    color = 0
    for m in mat:
        plt.plot(m.data_x, m.data_y, color=colors[color % len(colors)], linestyle=styles[(color // len(colors)) % len(styles)], label = m.type)
        color = (color + 1)
    plt.legend()
    plt.yscale(scale)

def plotForMatName(name):
    GMAT = []
    for i in MAT:
        if i.name == name:
            GMAT.append(i)
    if GMAT != []:
        GMAT.sort(key=lambda x: x.type, reverse=False)
        plot(GMAT)
        #plot(GMAT, scale='log')

exist = {}
for m in MAT:
    exist[m.name] = 0

#for x, y in exist.items():
#    plotForMatName(x)

for x in exist.keys():
    plotForMatName(x)

#plotForMatName("mat_memcpy")
#plotForMatName("mat_add")
#plotForMatName("mat_mul")
#plotForMatName("mat_jacobi")
#plotForMatName("mat_trans")
#plotForMatName("mat_trans-inplace")
#plotForMatName("mat_trans-block")



plt.show()
