/*  OpenACC micro benchmark
 *  Author: Josef Oškera
 *  Compiled: pgc++ 19.10-0, gcc 10.2.0
 *  Date: 10.12.2020
 */

#include "logger.hpp"
#include <map>
#include <iostream>
#include "mat.hpp"

using namespace std;

struct record {
	uint64_t width;
	double value;
	struct implement *imp;
	int touched;
};

typedef vector<struct record *> *records_t;




//rosults with Matrix size in MB
static string FOLDER = "./results";
//results with Matrix width in fields
static string FOLDER2 = "./results2";

static FILE *__result;
static FILE *__result2;

static bool record_on;
static string actual_id;
static string imp_name = "";

//static vector<records_t> records;
map<string, records_t> records;

static struct record *find_record(string id, uint64_t width){
	for(int i = 0; i < records[id]->size(); ++i){
		if((*records[id])[i]->width == width){
			return (*records[id])[i];
		}
	}
	return NULL;
}

void loggerSetImpName(string name){
    imp_name = name;
}

void loggerSetup(string id, std::string suffix, std::string header, std::string header2, bool recordOn){
    if(__result){
        loggerClose();
    }

    record_on = recordOn;
    actual_id = id;

    printf("\nTEST: %s\n", id.c_str());
    string tmp = FOLDER + "/" + id +"_" + suffix;
    string tmp2;
    if(record_on){
        tmp2 = FOLDER2 + "/result_imp/" + id + "_" + suffix;
    } else{
        __result = fopen(tmp.c_str(), "w");
        fprintf(__result, "%s\n", header.c_str());
        tmp2 = FOLDER2 + "/" + id + "_" + suffix;
    }
    __result2 = fopen(tmp2.c_str(), "w");

    fprintf(__result2, "%s\n", header2.c_str());
    printf("%s\n", header.c_str());

	if (records.find(id) == records.end()){
		//records[id] = new records_t();
		records[id] = new vector<struct record *>;
	}
}

void logger(uint64_t width, double value){
    if(!record_on){
        fprintf(__result, "%lf ", width * width * sizeof(float) * 1.0 / 1000000.0);
    }
    fprintf(__result2, "%lu ", width);
    printf("%lf ", width * width * sizeof(float) * 1.0 / 1000000.0);


    if(!record_on){
        fprintf(__result, "%lf\n", value);
    }
    fprintf(__result2, "%lf\n", value);
    printf("%lf\t", value);
    //printf("MAT width %lu\t\t", width);
    printf("MAT width %lu, MAT size %lu\t\t", width, width * width);
}
void logger(uint64_t width, double value, string id, struct implement *imp){
    logger(width, value);

    if(record_on){
        cout << "logger: " << imp->name << ": ";
	    struct record *tmp = find_record(id, width);
	    if(tmp){
		    if(tmp->value < value){
                cout << "update T";
			    tmp->value = value;
			    tmp->imp = imp;
		    } else {
                cout << "update F";
            }
		    tmp->touched++;
	    } else {
		    struct record *tmp2 = new struct record;
            tmp2->width = width;
            tmp2->value = value;
            tmp2->imp = imp;
            tmp2->touched = 1;
		    records[id]->push_back(tmp2);
            cout << "new";
            //printf("imp %p", imp);
	    }
        cout << "\t";
    }
}

void loggerClose(){
    if(__result){
        fclose(__result);
        __result = NULL;
    }
    if(__result2){
    	fclose(__result2);
    	__result2 = NULL;
    }
    imp_name = "";
    printf("\n");
}


void* loggerGetBestImp(string id, uint64_t width){
	for (auto a : *records[id]){
		if(a->touched == 1){
			continue;
		}
		if (a->width >= width){
            cout << "logger found: " << a->imp->name << "\t";
			return a->imp->func;
		}
	}
    cout << "logger found: nothing\t";
	return NULL;
}

void* loggerGetBestImp(uint64_t width){
    return loggerGetBestImp(actual_id, width);
}

void loggerDebug(){
    cout << "/////////////////////////////////////" << endl;
	map<string, records_t>::iterator it = records.begin();
	for(; it != records.end(); ++it){
        struct implement *imp = (*it->second)[0]->imp;
		cout << "logger: " << it->first << "  \timp: " << imp->name << " | ";
        printf("vector %p  imp %p\n", it->second, imp);
    }
    cout << "/////////////////////////////////////" << endl;
}
