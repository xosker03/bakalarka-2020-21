/*  OpenACC micro benchmark
 *  Author: Josef Oškera
 *  Compiled: pgc++ 19.10-0, gcc 10.2.0
 *  Date: 10.12.2020
 */

#include "mat.hpp"
#include "logger.hpp"

double matMemcpy(uint64_t size, uint64_t iter, struct implement *imp)
{
	(void) iter;
	(void) imp;

    //side of matrix is sqrt(max size / sizeof(float))
	uint64_t mat_sizeof = size;
	uint64_t mat_size = mat_sizeof / sizeof(float);
	uint64_t mat_side = sqrt(mat_size);
	mat_size = mat_side * mat_side;
	mat_sizeof = mat_size * sizeof(float);

    //Matrices allocations
    GpuMem<float> data(mat_size);

    data.createOnDev();

    //Test section
    //data copy to device (if you don't use CPU)
    timerStart();
    data.cpyTD();
    timerStop();

    data.deleteOnDev();

    //result print
    double gbs = (mat_sizeof) / (((double)timer_us) / 1000000);
    gbs /= 1000000000;
	logger(mat_side, gbs);


	printf("\n");
    return (((double)timer_us) / 1000000);
}
