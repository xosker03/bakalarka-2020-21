/*  OpenACC micro benchmark
 *  Author: Josef Oškera
 *  Compiled: pgc++ 19.10-0, gcc 10.2.0
 *  Date: 10.12.2020
 */

#ifndef _TEMPLATES_H_
#define _TEMPLATES_H_

#include <stdint.h>

template<class TYPE>
class GpuMem
{
public:
    GpuMem(uint64_t size);
    GpuMem(TYPE * ptr, uint64_t size);
    GpuMem(GpuMem<TYPE> &other);
    //GpuMem(GpuMem<TYPE> &&other);
    virtual ~GpuMem();

    TYPE * data();
    uint64_t size();
    bool onDev();
    void createOnDev();
    void deleteOnDev();
    //copy to device
    void cpyTD();
    void cpyTD(uint64_t from, uint64_t count);
    void cpyTDasync(uint64_t from, uint64_t count, uint64_t async);
    //copy to host
    void cpyTH();
    void cpyTH(uint64_t from, uint64_t count);
    void cpyTHasync(uint64_t from, uint64_t count, uint64_t async);

    TYPE & operator [] (int i);
    bool operator == (GpuMem<TYPE> &other);
    bool operator != (GpuMem<TYPE> &other);

private:
    TYPE *__data;
    uint64_t __size;
    bool __onDevice;
};


#endif
