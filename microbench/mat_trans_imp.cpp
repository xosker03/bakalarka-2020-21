/*  OpenACC micro benchmark
 *  Author: Josef Oškera
 *  Compiled: pgc++ 19.10-0, gcc 10.2.0
 *  Date: 10.12.2020
 */

#include "mat.hpp"
#include "logger.hpp"

void matTransAuto(struct matParams par);
void matTransTile32(struct matParams par);
void matTransDumb(struct matParams par);


std::vector<struct implement> matTransImp = {
	{ "mat_trans_auto", (void *) matTransAuto, true },
	{ "mat_trans_tile32", (void *) matTransTile32, false },
  { "mat_trans_dumb", (void *) matTransDumb, false }
};

void matTransAuto(struct matParams par)
{
	void (*implementation)(struct matParams) = (void (*)(struct matParams)) loggerGetBestImp(par.mat_side);
    if (!implementation){
    	implementation = (void (*)(struct matParams)) matTransImp[1].func; //implementation = matTransBasic;
    }
	implementation(par);
}
//-----------------------------------------------------------------------------------
void matTransTile32(struct matParams par)
{
	uint64_t iter = par.iter;
	uint64_t mat_side = par.mat_side;

	//support pointer to matrix data
	float *mt_a = par.mat_a->data();
	float *mt_b = par.mat_b->data();

	#pragma acc data present(mt_a, mt_b)
    {
        timerStart();
        for(uint64_t j = 0; j < iter; ++j)
        {
            #pragma acc parallel loop gang, worker tile(32)
            for(uint64_t y = 0; y < mat_side; ++y)
            {
                #pragma acc loop vector
                for(uint64_t x = 0; x < mat_side; ++x)
                {
                    mt_b[y * mat_side + x] = mt_a[x * mat_side + y];
                }
            }
        }
        timerStop();
    }
}
//-----------------------------------------------------------------------------------
void matTransDumb(struct matParams par)
{
  uint64_t iter = par.iter;
  uint64_t mat_side = par.mat_side;

  //support pointer to matrix data
  float *mt_a = par.mat_a->data();
  float *mt_b = par.mat_b->data();

  #pragma acc data present(mt_a, mt_b)
    {
        timerStart();
        for(uint64_t j = 0; j < iter; ++j)
        {
            #pragma acc parallel loop collapse(2)
            for(uint64_t y = 0; y < mat_side; ++y)
            {
                for(uint64_t x = 0; x < mat_side; ++x)
                {
                    mt_b[y * mat_side + x] = mt_a[x * mat_side + y];
                }
            }
        }
        timerStop();
    }
}
