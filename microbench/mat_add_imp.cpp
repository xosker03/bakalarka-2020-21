/*  OpenACC micro benchmark
 *  Author: Josef Oškera
 *  Compiled: pgc++ 19.10-0, gcc 10.2.0
 *  Date: 10.12.2020
 */

#include "mat.hpp"
#include "logger.hpp"

void matAddAuto(struct matParams par);
void matAddBasic(struct matParams par);


std::vector<struct implement> matAddImp = {
	{ "mat_add_auto", (void *) matAddAuto, true },
	{ "mat_add_basic", (void *) matAddBasic, false }
};


void matAddAuto(struct matParams par){
	void (*implementation)(struct matParams) = (void (*)(struct matParams)) loggerGetBestImp(par.mat_side);
	    if (!implementation){
	    	implementation = (void (*)(struct matParams)) matAddImp[1].func; //implementation = matAddBasic;
    }
	implementation(par);
}


void matAddBasic(struct matParams par)
{
	uint64_t iter = par.iter;
	uint64_t mat_side = par.mat_side;

	//support pointer to matrix data
	float *mt_a = par.mat_a->data();
	float *mt_b = par.mat_b->data();
	float *mt_c = par.mat_c->data();

	//#pragma acc data copy(mt_a[0:mat_size], mt_b[0:mat_size], mt_c[0:mat_size])
	#pragma acc data present(mt_a, mt_b, mt_c)
	{
		timerStart();
		for(uint64_t j = 0; j < iter; ++j)
		{
			#pragma acc parallel loop gang worker vector
			for(uint64_t i = 0; i < mat_side * mat_side; ++i)
			{
				mt_c[i] = mt_a[i] + mt_b[i];
			}
		}
	}
	timerStop();
}
