/*  OpenACC micro benchmark
 *  Author: Josef Oškera
 *  Compiled: pgc++ 19.10-0, gcc 10.2.0
 *  Date: 10.12.2020
 */

#ifndef _LOGGER_H_
#define _LOGGER_H_

#include <stdint.h>
#include <string>
#include <vector>


void loggerSetImpName(std::string name);
void loggerSetup(std::string id, std::string suffix, std::string header, std::string header2, bool recordOn);
void logger(uint64_t width, double value);
void logger(uint64_t width, double value, std::string id, struct implement *imp);
void loggerClose();

void* loggerGetBestImp(std::string id, uint64_t width);
void* loggerGetBestImp(uint64_t width);

void loggerDebug();



#endif
