/*  OpenACC micro benchmark
 *  Author: Josef Oškera
 *  Compiled: pgc++ 19.10-0, gcc 10.2.0
 *  Date: 10.12.2020
 */

#include "templates.hpp"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

template<class TYPE> GpuMem<TYPE>::GpuMem(uint64_t size)
{
    this->__data = (TYPE *) calloc(size, sizeof(TYPE));
    if(this->__data == NULL)
    {
        throw "GpuMem::GpuMem nedostatek pameti";
    }
    this->__onDevice = false;
    this->__size = size;
}

template<class TYPE> GpuMem<TYPE>::GpuMem(TYPE * ptr, uint64_t size)
{
    this->__data = ptr;
    if(this->__data == NULL)
    {
        throw "GpuMem::GpuMem NULL ptr";
    }
    this->__onDevice = false;
    this->__size = size;
}

template<class TYPE> GpuMem<TYPE>::GpuMem(GpuMem<TYPE> &other)
{
    if(other.__onDevice == true)
    {
        other.cpyTH();
    }
    __size = other.__size;
    __onDevice = false;
    __data = (TYPE *) calloc(__size, sizeof(TYPE));
    if(__data == NULL)
    {
        throw "GpuMem::GpuMem nedostatek pameti";
    }
    for(uint64_t i = 0; i < __size; ++i)
    {
        __data[i] = other.__data[i];
    }
}

template<class TYPE> GpuMem<TYPE>::~GpuMem()
{
    if(__data != NULL)
    {
        if(this->__onDevice)
        {
            this->deleteOnDev();
        }
        free(this->__data);
    }
}


template<class TYPE> TYPE * GpuMem<TYPE>::data()
{
    return this->__data;
}

template<class TYPE> uint64_t GpuMem<TYPE>::size()
{
    return this->__size;
}

template<class TYPE> bool GpuMem<TYPE>::onDev()
{
    return this->__onDevice;
}

template<class TYPE> void GpuMem<TYPE>::createOnDev()
{
    #pragma acc enter data create(this)
    #pragma acc update device(this)
    //#pragma acc enter data copyin(this)
    #pragma acc enter data create(__data[0:__size])
    this->__onDevice = true;
}

template<class TYPE> void GpuMem<TYPE>::deleteOnDev()
{
    #pragma acc exit data delete(__data[0:__size])
    #pragma acc exit data delete(this)
    this->__onDevice = false;
}


template<class TYPE> void GpuMem<TYPE>::cpyTD()
{
    this->cpyTD(0, this->__size);
}
template<class TYPE> void GpuMem<TYPE>::cpyTD(uint64_t from, uint64_t count)
{
    if(this->__onDevice == false)
    {
        this->createOnDev();
    }
    #pragma acc update device(this)
    #pragma acc update device(__data[from:count])
}
template<class TYPE> void GpuMem<TYPE>::cpyTDasync(uint64_t from, uint64_t count, uint64_t async)
{
    if(this->__onDevice == false)
    {
        this->createOnDev();
    }
    #pragma acc update device(this) async(async)
    #pragma acc update device(__data[from:count]) async(async)
}


template<class TYPE> void GpuMem<TYPE>::cpyTH()
{
    this->cpyTH(0, this->__size);
}
template<class TYPE> void GpuMem<TYPE>::cpyTH(uint64_t from, uint64_t count)
{
    if(this->__onDevice == false)
    {
        throw "GpuMem::cpyTH neexistuje na deevice";
    }
    #pragma acc update self(this)
    #pragma acc update self(__data[from:count])
}
template<class TYPE> void GpuMem<TYPE>::cpyTHasync(uint64_t from, uint64_t count, uint64_t async)
{
    if(this->__onDevice == false)
    {
        throw "GpuMem::cpyTHasync neexistuje na deevice";
    }
    #pragma acc update self(this) async(async)
    #pragma acc update self(__data[from:count]) async(async)
}


template<class TYPE> TYPE & GpuMem<TYPE>::operator [] (int i)
{
    return this->__data[i];
}

template<class TYPE> bool GpuMem<TYPE>::operator == (GpuMem<TYPE> &other)
{
    bool result = true;

    if(__size != other.__size)
    {
        return false;
    }

    for(uint64_t i = 0; i < __size; ++i)
    {
        if(__data[i] == other.__data[i])
        {
        }
        else
        {
            result = false;
            break;
        }
    }
    return result;
}

template<class TYPE> bool GpuMem<TYPE>::operator != (GpuMem<TYPE> &other)
{
    return ! (*this == other);
}



template class GpuMem<float>;
