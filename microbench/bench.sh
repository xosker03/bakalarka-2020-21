#!/bin/bash

lscpu
echo ""
lspci | grep -i nvidia
echo ""

function line {
	echo ////////////////////////////////////////////////////////////////////////////////////////
	echo ////////////////////////////////////////////////////////////////////////////////////////
	echo ////////////////////////////////////////////////////////////////////////////////////////
}

function GCC {
	#GCC
	line && make clean && make gcc
	taskset 1  ./tester "GCC" 50 512 10	#add jacobi
	taskset 1  ./tester "GCC" 44 512 1		#mul trans
}

function CPU {
	#CPU
	line && make clean && make cpu
	taskset 1  ./tester "CPU" 50 512 10	#add jacobi
	taskset 1  ./tester "CPU" 44 512 1		#mul trans
}

function MULTI {
	#MULTI
	line && make clean && make multi 
	./tester "MULTI" 50 512 100			#add jacobi
	./tester "MULTI" 44 512 1				#mul trans
}

function GPU {
#GPU
if [[ "$1" != "--nogpu" ]]
then
	line && make clean && make gpu
	taskset 1  ./tester "GPU" 50 512 10	#add jacobi
	taskset 1  ./tester "GPU" 44 512 1		#mul trans
	taskset 1  ./tester "GPU" 1 256 0		#memcopy
fi
}


GCC
CPU
MULTI
GPU


echo ""
echo HOTOVO


