/**
 * @file      cpu_acc.cpp
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Soubor implementace C++/OpenACC kernelů pro hlavní výpočet
 *
 * @version   heatsim 1.0
 *
 * @date      19.5.2021
 */

#include "cpu_acc.hpp"

#include "main.hpp"
#include "helpers.hpp"

#ifdef GPU_ACC
	#include <accelmath.h>
#else
	#include <math.h>
#endif
////////////////// HOMOGENOUS ////////////////////////

//p_term = - obj.perfusion_coeff .* obj.IT( kappa .* obj.FT(obj.T - obj.blood_ambient_temperature) );
void a_perfusion(int R, int C, float *p_term, float *T, float *kappa,
		FloatComplex *tmpFft, float coef, float temp, float idivider)
{
	#pragma acc parallel loop
	for(int r = 0; r < R; ++r)
	{
		p_term[r] = T[r] - temp;
	}

	fftR2C(p_term, tmpFft);

	#pragma acc parallel loop
	for(int c = 0; c < C; ++c)
	{
		tmpFft[c] *= kappa[c];
	}

	fftC2R(tmpFft, p_term);
	

	#pragma acc parallel loop
	for(int r = 0; r < R; ++r)
	{
		p_term[r] *= -coef * idivider;
	}

}


void a_after_fft(int N, FloatComplex *tmpFft, float *k, float *kappa){
	#pragma acc parallel loop
	for(int c = 0; c < N; ++c)
	{
		tmpFft[c] = -k[c] * kappa[c] * tmpFft[c];
	}

}

void a_after_tff(int N, float *tmpTff, float idivider){
	#pragma acc parallel loop
	for(int i = 0; i < N; ++i)
	{
		tmpTff[i] *= idivider;
	}
}

void a_d_term(int N, float *d_term, float *tmpTff, float diffusion_p1, float diffusion_p2){
	#pragma acc parallel loop
	for(int i = 0; i < N; ++i)
	{
		d_term[i] = diffusion_p1 * diffusion_p2 * tmpTff[i];
	}
}

void a_T(int N, float *T, float *d_term, float dt){
	#pragma acc parallel loop
	for(int i = 0; i < N; ++i)
	{
		T[i] += dt * d_term[i];
	}
}
void a_T(int N, float *T, float *d_term, float *p_term, float *q_term, float dt){
	#pragma acc parallel loop
	for(int i = 0; i < N; ++i)
	{
		T[i] += dt * (d_term[i] + p_term[i] + q_term[i]);
	}
}



//obj.cem43 = obj.cem43 + dt ./ 60 .* ( 0.5 .* (obj.T >= 43) + 0.25 .* (obj.T >= 37 & obj.T < 43 ) ).^(43 - obj.T);
void a_cem43(int N, float *cem43, float *T, float dt){
	#pragma acc parallel loop
	for(int i = 0; i < N; ++i)
	{
		float tmp = 0.5 * (T[i] >= 43) + 0.25 * ((T[i] >= 37) & (T[i] < 43));
		float power = pow(tmp, 43.0 - T[i]);
		cem43[i] = cem43[i] + dt * (1.0/60) * power;
	}
}


////////////////// NO HOMOGENOUS ////////////////////////
//p_term = - obj.perfusion_coeff .* obj.IT( kappa .* obj.FT(obj.T - obj.blood_ambient_temperature) );
void a_perfusion_noh(int R, int C, float *p_term, float *T, float *kappa,
		FloatComplex *tmpFft, float *coef, float *temp, float idivider)
{
	#pragma acc parallel loop
	for(int r = 0; r < R; ++r)
	{
		p_term[r] = T[r] - temp[r];
	}

	fftR2C(p_term, tmpFft);

	#pragma acc parallel loop
	for(int c = 0; c < C; ++c)
	{
		tmpFft[c] *= kappa[c];
	}

	fftC2R(tmpFft, p_term);
	

	#pragma acc parallel loop
	for(int r = 0; r < R; ++r)
	{
		p_term[r] *= -coef[r] * idivider;
	}

}



void a_fft_x_noh(int N, FloatComplex *fft, FloatComplex *tmpFft, FloatComplex *deriv)
{
	#pragma acc parallel loop
	for(int c = 0; c < N; ++c)
	{
		fft[c] = tmpFft[c] * deriv[c];
	}
}

void a_tff_x_noh(int N, float *tff, float idivider, float diffusion_p2)
{
	#pragma acc parallel loop
	for(int r = 0; r < N; ++r)
	{
		tff[r] *= idivider * diffusion_p2;
	}
}

void a_fft_x_2_noh(int N, FloatComplex *fft, FloatComplex *deriv)
{
	#pragma acc parallel loop
	for(int c = 0; c < N; ++c)
	{
		fft[c] *= deriv[c];
	}
}

void a_pre_d_term_noh(int N, float *tff, float idivider)
{
	#pragma acc parallel loop
	for(int r = 0; r < N; ++r)
	{
		tff[r] *= idivider;
	}
}




void a_d_term_noh(int N, float *d_term, float *diffusion_p1_noh, float *tff_1, float *tff_2)
{
	#pragma acc parallel loop
	for(int r = 0; r < N; ++r)
	{
		d_term[r] = diffusion_p1_noh[r] * ( (tff_1[r]) + (tff_2[r]));
	}
}
void a_d_term_noh(int N, float *d_term, float *diffusion_p1_noh, float *tff_1, float *tff_2, float *tff_3)
{
	#pragma acc parallel loop
	for(int r = 0; r < N; ++r)
	{
		d_term[r] = diffusion_p1_noh[r] * ( (tff_1[r]) + (tff_2[r]) + (tff_3[r]));
	}
}
void a_d_term_noh(int N, float *d_term, float diffusion_p1, float *tff_1, float *tff_2)
{
	#pragma acc parallel loop
	for(int r = 0; r < N; ++r)
	{
		d_term[r] = diffusion_p1 * ( (tff_1[r]) + (tff_2[r]));
	}
}
void a_d_term_noh(int N, float *d_term, float diffusion_p1, float *tff_1, float *tff_2, float *tff_3)
{
	#pragma acc parallel loop
	for(int r = 0; r < N; ++r)
	{
		d_term[r] = diffusion_p1 * ( (tff_1[r]) + (tff_2[r]) + (tff_3[r]));
	}
}





void a_T_noh(int N, float *T, float dt, float *d_term, float *p_term, float *q_term)
{
	#pragma acc parallel loop
	for(int r = 0; r < N; ++r)
	{
		T[r] += dt * (d_term[r] + p_term[r] + q_term[r]);
	}
}












