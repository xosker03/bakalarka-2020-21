/**
 * @file      nohomo.hpp
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Rozšíření souboru main.cpp
 *
 * @version   heatsim 1.0
 *
 * @date      19.5.2021
 */

#ifndef GPU_CUDA

int R = get_size(FullDim);
int C = get_size(ReducedDim);

//p_term = - obj.perfusion_coeff .* obj.IT( kappa .* obj.FT(obj.T - obj.blood_ambient_temperature) );
if(use_perfusion){
	a_perfusion_noh(R, C, p_term, T, kappa,
		tmpFft, mem_perfusion_coeff.data(), mem_blood_ambient_temperature.data(), idivider);
}

fftR2C(T, tmpFft);


/// d_term = obj.diffusion_p1 .* ...
///                       ( obj.IT( deriv_x .* obj.FT( obj.diffusion_p2 .* obj.IT( deriv_x .* T_FT ) ) ) + ...
///                         obj.IT( deriv_y .* obj.FT( obj.diffusion_p2 .* obj.IT( deriv_y .* T_FT ) ) ) + ...
///                         obj.IT( deriv_z .* obj.FT( obj.diffusion_p2 .* obj.IT( deriv_z .* T_FT ) ) ) );

/// deriv_x .* T_FT
a_fft_x_noh(C, fft_1, tmpFft, deriv_x);
a_fft_x_noh(C, fft_2, tmpFft, deriv_y);
if(dimension_count == 3)
	a_fft_x_noh(C, fft_3, tmpFft, deriv_z);


///obj.IT( deriv_x .* T_FT )
fftC2R(fft_1, tff_1);
fftC2R(fft_2, tff_2);
if(dimension_count == 3)
	fftC2R(fft_3, tff_3);

///obj.diffusion_p2 .* obj.IT( deriv_x .* T_FT )
a_tff_x_noh(R, tff_1, idivider, diffusion_p2);
a_tff_x_noh(R, tff_2, idivider, diffusion_p2);
if(dimension_count == 3)
	a_tff_x_noh(R, tff_3, idivider, diffusion_p2);


///obj.FT( obj.diffusion_p2 .* obj.IT( deriv_x .* T_FT ) )
fftR2C(tff_1, fft_1);
fftR2C(tff_2, fft_2);
if(dimension_count == 3)
	fftR2C(tff_3, fft_3);


///deriv_x .* obj.FT( obj.diffusion_p2 .* obj.IT( deriv_x .* T_FT ) )
a_fft_x_2_noh(C, fft_1, deriv_x);
a_fft_x_2_noh(C, fft_2, deriv_y);
if(dimension_count == 3)
	a_fft_x_2_noh(C, fft_3, deriv_z);


///obj.IT( deriv_x .* obj.FT( obj.diffusion_p2 .* obj.IT( deriv_x .* T_FT ) ) )
fftC2R(fft_1, tff_1);
fftC2R(fft_2, tff_2);
if(dimension_count == 3)
	fftC2R(fft_3, tff_3);


a_pre_d_term_noh(R, tff_1, idivider);
a_pre_d_term_noh(R, tff_2, idivider);
if(dimension_count == 3)
	a_pre_d_term_noh(R, tff_3, idivider);

if(dimension_count != 3){
	a_d_term_noh(R, d_term, diffusion_p1_noh, tff_1, tff_2);
} else {
	a_d_term_noh(R, d_term, diffusion_p1_noh, tff_1, tff_2, tff_3);
}

a_T_noh(R, T, dt, d_term, p_term, q_term);

a_cem43(R, cem43, T, dt);
	

#else


int R = get_size(FullDim);
int C = get_size(ReducedDim);

//p_term = - obj.perfusion_coeff .* obj.IT( kappa .* obj.FT(obj.T - obj.blood_ambient_temperature) );
if(use_perfusion)
	c_perfusion_noh(R, C, mem_p_term.g_data(), mem_T.g_data(), mem_kappa.g_data(),
			mem_tmpFft.g_data(), mem_perfusion_coeff.g_data(), mem_blood_ambient_temperature.g_data(), idivider);

fftR2C(mem_T.g_data(), mem_tmpFft.g_data());

/// deriv_x .* T_FT
c_fft_x_noh(C, mem_fft_1.g_data(), mem_tmpFft.g_data(), mem_deriv_x.g_data());
c_fft_x_noh(C, mem_fft_2.g_data(), mem_tmpFft.g_data(), mem_deriv_y.g_data());
if(dimension_count == 3)
	c_fft_x_noh(C, mem_fft_3.g_data(), mem_tmpFft.g_data(), mem_deriv_z.g_data());

///obj.IT( deriv_x .* T_FT )
fftC2R(mem_fft_1.g_data(), mem_tff_1.g_data());
fftC2R(mem_fft_2.g_data(), mem_tff_2.g_data());
if(dimension_count == 3)
	fftC2R(mem_fft_3.g_data(), mem_tff_3.g_data());

///obj.diffusion_p2 .* obj.IT( deriv_x .* T_FT )
c_tff_x_noh(R, mem_tff_1.g_data(), idivider, diffusion_p2);
c_tff_x_noh(R, mem_tff_2.g_data(), idivider, diffusion_p2);
if(dimension_count == 3)
	c_tff_x_noh(R, mem_tff_3.g_data(), idivider, diffusion_p2);

///obj.FT( obj.diffusion_p2 .* obj.IT( deriv_x .* T_FT ) )
fftR2C(mem_tff_1.g_data(), mem_fft_1.g_data());
fftR2C(mem_tff_2.g_data(), mem_fft_2.g_data());
if(dimension_count == 3)
	fftR2C(mem_tff_3.g_data(), mem_fft_3.g_data());

///deriv_x .* obj.FT( obj.diffusion_p2 .* obj.IT( deriv_x .* T_FT ) )
c_fft_x_2_noh(C, mem_fft_1.g_data(), mem_deriv_x.g_data());
c_fft_x_2_noh(C, mem_fft_2.g_data(), mem_deriv_y.g_data());
if(dimension_count == 3)
	c_fft_x_2_noh(C, mem_fft_3.g_data(), mem_deriv_z.g_data());

///obj.IT( deriv_x .* obj.FT( obj.diffusion_p2 .* obj.IT( deriv_x .* T_FT ) ) )
fftC2R(mem_fft_1.g_data(), mem_tff_1.g_data());
fftC2R(mem_fft_2.g_data(), mem_tff_2.g_data());
if(dimension_count == 3)
	fftC2R(mem_fft_3.g_data(), mem_tff_3.g_data());

///d_term = obj.diffusion_p1 .*
c_pre_d_term_noh(R, mem_tff_1.g_data(), idivider);
c_pre_d_term_noh(R, mem_tff_2.g_data(), idivider);
if(dimension_count == 3)
	c_pre_d_term_noh(R, mem_tff_3.g_data(), idivider);

if(dimension_count != 3){
	c_d_term_noh(R, mem_d_term.g_data(), mem_diffusion_p1.g_data(), 
			mem_tff_1.g_data(), mem_tff_2.g_data());
} else {
	c_d_term_noh(R, mem_d_term.g_data(), mem_diffusion_p1.g_data(), 
			mem_tff_1.g_data(), mem_tff_2.g_data(), mem_tff_3.g_data());
}

c_T_noh(R, mem_T.g_data(), mem_p_term.g_data(), mem_q_term.g_data(), dt, mem_d_term.g_data());

c_cem43(R, mem_cem43.g_data(), mem_T.g_data(), dt);

#endif


